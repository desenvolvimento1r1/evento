<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonAdmin for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin;

use Admin\Controller\BancoContaController;
use Admin\Controller\ClienteController;
use Admin\Controller\ContaReceberController;
use Admin\Controller\CrmInteressadoController;
use Admin\Controller\FuncionarioController;
use Admin\Controller\IndexController;
use Admin\Controller\InicioController;
use Admin\Controller\UsuarioController;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/admin[/:controller[/:action]]',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => InvokableFactory::class,
            InicioController::class => InvokableFactory::class,
            UsuarioController::class => InvokableFactory::class,
            FuncionarioController::class => InvokableFactory::class,
            ClienteController::class => InvokableFactory::class,
            CrmInteressadoController::class => InvokableFactory::class,
            ContaReceberController::class => InvokableFactory::class,
            BancoContaController::class => InvokableFactory::class,
        ],
        'aliases' => [
            'index' => IndexController::class,
            'inicio' => InicioController::class,
            'usuario' => UsuarioController::class,
            'funcionario' => FuncionarioController::class,
            'cliente' => ClienteController::class,
            'crm-interessado' => CrmInteressadoController::class,
            'conta-receber' => ContaReceberController::class,
            'banco-conta' => Controller\BancoContaController::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404-admin',
        'exception_template' => 'error/index-admin',
        'template_map' => [
            'layout/login' => __DIR__ . '/../view/layout/login.phtml',
            'layout/admin' => __DIR__ . '/../view/layout/admin.phtml',
            'layout/permissao' => __DIR__ . '/../view/layout/permissao.phtml',
            'admin/index/index' => __DIR__ . '/../view/admin/index/index.phtml',
            'error/404-admin' => __DIR__ . '/../view/error/404.phtml',
            'error/index-admin' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
