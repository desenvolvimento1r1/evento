<div class="menu">
    <ul class="list">
        <li class="header">MAIN NAVIGATION</li>
        <li>
            <a href="<?= $this->serverUrl("/admin/inicio") ?>">
                <i class="material-icons">home</i>
                <span>Home</span>
            </a>
        </li>
        <li class="active">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">group</i>
                <span>Clientes</span>
            </a>
            <ul class="ml-menu">
                <li class="active">
                    <a href="<?= $this->serverUrl("/admin/cliente") ?>">Cadastro de Clientes</a>
                </li>
                <!--                <li>
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <span>Infobox</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li>
                                            <a href="pages/widgets/infobox/infobox-1.html">Infobox-1</a>
                                        </li>
                                        <li>
                                            <a href="pages/widgets/infobox/infobox-2.html">Infobox-2</a>
                                        </li>
                                        <li>
                                            <a href="pages/widgets/infobox/infobox-3.html">Infobox-3</a>
                                        </li>
                                        <li>
                                            <a href="pages/widgets/infobox/infobox-4.html">Infobox-4</a>
                                        </li>
                                        <li>
                                            <a href="pages/widgets/infobox/infobox-5.html">Infobox-5</a>
                                        </li>
                                    </ul>
                                </li>-->
            </ul>
        </li>
        <li >
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">account_circle</i>
                <span>Usuários</span>
            </a>
            <ul class="ml-menu">
                <li>
                    <a href="<?= $this->serverUrl("/admin/usuario") ?>">Cadastro de Usuários</a>
                </li>
            </ul>
        </li>
    </ul>
</div>