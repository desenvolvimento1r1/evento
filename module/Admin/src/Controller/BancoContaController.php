<?php

namespace Admin\Controller;

use Base\Service\Connection;
use Base\Service\Tbbancoconta;
use Base\Service\Tbbancocontapermissao;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class BancoContaController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "banco-conta";
        $this->tituloPagina = "Contas Bancárias";
        $this->subTituloPagina = "Cadastro de Contas Bancárias";
    }

    public function indexAction() {
        $bancoConta = (new Tbbancoconta())->fetchAll();

        $viewModel = new ViewModel(array(
            'bancoConta' => $bancoConta
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_bancoconta = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_bancoconta)) {
            $Tbbancoconta = new Tbbancoconta();

            $dataPopulate = $Tbbancoconta->findOneById($id_bancoconta);

            if (!empty($dataPopulate)) {
                $arrPopulate['banco_conta'] = $dataPopulate;
            } else {
                $this->redirect()->toUrl('/admin/banco-conta/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }
        return new ViewModel(array(
            'id_bancoconta' => $id_bancoconta
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }
            $bancoConta = $this->request->getPost('banco_conta');

            $Tbbancoconta = new Tbbancoconta();

            if (empty($bancoConta['id_tenant'])) {
                $bancoConta['id_tenant'] = (new Container('admin'))->id_tenant;
            }

            $id = $Tbbancoconta->save($bancoConta, $Default);

            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id;
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerAction() {
        $Default = (new Connection)->getConnection();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');


            $Default->getPdo()->beginTransaction();

            $Tbbancoconta = new Tbbancoconta();
            $Tbbancoconta->removeAll(array('id_bancoconta' => $id), $Default);

            $Default->getPdo()->commit();

            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
