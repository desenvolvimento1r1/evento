<?php

namespace Admin\Controller;

use Base\Service\Tbusuariotenant;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\View\Model\ViewModel;

class IndexController extends CustomActionController {

    public function __construct() {
        $this->module = "login";
    }

    public function indexAction() {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    public function verificaLoginAction() {        
        $username = $this->params()->fromPost('username');
        $password = $this->params()->fromPost('password');

        try {

            $Tbusuariotenant = new Tbusuariotenant();
            $usuario = $Tbusuariotenant->fetchAll(array(
                'usuario' => $username,
                'senha' => $password,
            ));

            if (count($usuario) > 1) {
                Throw new Exception("Ocorreu um erro ao localizar o usuário, entre em contato com o suporte do sistema.");
            } else {
                if (!empty($usuario)) {
                    $usuario = current($usuario);
                    if ($usuario['st_ativo'] == 'N') {
                        Throw new Exception('Seu usuário esta com acesso bloqueado ao sistema, entre em contato com o administrador.');
                    }

                    $this->setAcl($usuario);
                    $return['type'] = 'success';
                    $return['message'] = 'Entrando';
                } else {
                    Throw new Exception('Usuário/senha não encontrados.');
                }
            }
        } catch (Exception $ex) {
            $return['type'] = 'error';
            $return['message'] = $ex->getMessage();
        }

//        print_r($return);die;

        $json = new Json();
        echo $json->encode($return);
        die;
    }

//    public function cadastrarAction() {
//        $data = $this->params()->fromPost();
//
//        $Tbusuariotenant = new Tbusuariotenant();
//
//        try {
//            $this->dd($data);
//            $return['type'] = 'success';
//            $return['message'] = 'Entrando';
//        } catch (Exception $ex) {
//            $return['type'] = 'error';
//            $return['message'] = $ex->getMessage();
//        }
//
//
//        $json = new Json();
//        echo $json->encode($return);
//        die;
//    }

    public function loginFbAction() {
        $data = $this->params()->fromPost();

//        (new \Base\Controller\BaseController())->dd(true, $data);

        if (!isset($data['middle_name'])) {
            $data['middle_name'] = '';
        }

        if (!isset($data['last_name'])) {
            $data['last_name'] = '';
        }

        if (!isset($data['email'])) {
            $data['email'] = '';
        }

        $Tbusuariotenant = new Tbusuariotenant();


        $usuarioFb = $Tbusuariotenant->fetchAll(array('fb_userid' => $data['id'], 'or_tx_email' => $data['email']));

        try {
            if (empty($usuarioFb)) {
                throw new Exception("Não encontramos nenhuma conta vínculada ao seu facebook. Favor logar com o seu usuário e senha e víncular a sua conta.");
            } else {
                $this->setAcl();

                $return['type'] = 'success';
                $return['message'] = 'Entrando';
            }
        } catch (Exception $ex) {
            $return['type'] = 'error';
            $return['message'] = $ex->getMessage();
        }


        $json = new Json();
        echo $json->encode($return);
        die;
    }

    private function setAcl($user) {

        $containerTenant = new \Zend\Session\Container('tenant');
        $containerTenant->id_tenant = $user['id_tenant'];
        
        $container = new \Zend\Session\Container('admin');
        $container->id_tenant = $user['id_tenant'];

        $container = new \Zend\Session\Container('usuarioLogado');
        $container->id_usuariotenant = $user['id_usuariotenant'];
        $container->tx_email = $user['tx_email'];
        $container->tx_login = $user['tx_login'];
        $container->tx_nomeusuario = $user['tx_nomeusuario'];

//        $acl = new Acl();
//
//        $acl->addResource('cadastro-usuario')
//                ->addResource('alterar-usuario');
//
//        $acl->addRole('usuario');
//
//        $acl->allow('usuario', 'cadastro-usuario');
//
//        echo var_export($acl->isAllowed('usuario', 'cadastro-usuario'));
//        die;
    }

}
