<?php

namespace Admin\Controller;

use Base\Controller\BaseController;
use Base\Controller\UploadController;
use Base\Service\Connection;
use Base\Service\Tbcliente;
use Base\Service\Tbclienteemail;
use Base\Service\Tbclienteendereco;
use Base\Service\Tbclienteinteressado;
use Base\Service\Tbclienteinteressadoemail;
use Base\Service\Tbclienteinteressadoendereco;
use Base\Service\Tbclienteinteressadotelefone;
use Base\Service\Tbclientetelefone;
use Base\Service\Tbinteracaoclienteinteressado;
use Base\Service\Tbinteracaoclienteinteressadoanexo;
use Base\Service\Tbtenant;
use Base\Service\Tbusuariotenant;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class CrmInteressadoController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "crm-interessado";
        $this->tituloPagina = "CRM - Interessados";
        $this->subTituloPagina = "Cadastro de Interessados";
    }

    public function indexAction() {
        $clienteInteressados = (new Tbclienteinteressado())->fetchAll(['st_origemcliente' => 'N']);

        $viewModel = new ViewModel(array(
            'clientes' => $clienteInteressados
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_clienteinteressado = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_clienteinteressado)) {
            $Tbclienteinteressado = new Tbclienteinteressado();

            $dataPopulate = $Tbclienteinteressado->findOneById($id_clienteinteressado);

            if (!empty($dataPopulate)) {
                if (!empty($dataPopulate['dt_primeirocontato'])) {
                    $dataPopulate['dt_primeirocontato'] = (new BaseController())->reverseDate($dataPopulate['dt_primeirocontato']);
                }
                if (!empty($dataPopulate['dt_nascimento'])) {
                    $dataPopulate['dt_nascimento'] = (new BaseController())->reverseDate($dataPopulate['dt_nascimento']);
                }
                $arrPopulate['clienteInteressado'] = $dataPopulate;
            } else {
                $this->redirect()->toUrl('/admin/crm-interessado/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }

        return new ViewModel(array(
            'id_clienteinteressado' => $id_clienteinteressado
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }
            $clienteInteressado = $this->request->getPost('clienteInteressado');
            $clienteInteressadoTelefone = $this->request->getPost('clienteinteressado-telefone');
            $clienteInteressadoEmail = $this->request->getPost('clienteinteressado-email');
            $clienteInteressadoEndereco = $this->request->getPost('clienteinteressado-endereco');

            $Tbclienteinteressado = new Tbclienteinteressado();

            if (!empty($clienteInteressado['id_clienteinteressado'])) {
                $clienteInteressado['id_clienteinteressado'] = (int) $clienteInteressado['id_clienteinteressado'];
            }

            if (empty($clienteInteressado['id_tenant'])) {
                $clienteInteressado['id_tenant'] = (new Container('admin'))->id_tenant;
            }

            if (!empty($clienteInteressado['dt_primeirocontato'])) {
                $clienteInteressado['dt_primeirocontato'] = (new BaseController())->reverseDate($clienteInteressado['dt_primeirocontato']);
            }

            if (!empty($clienteInteressado['dt_nascimento'])) {
                $clienteInteressado['dt_nascimento'] = (new BaseController())->reverseDate($clienteInteressado['dt_nascimento']);
            }

            /*
             * Salva o o cliente
             */
            $id = $Tbclienteinteressado->save($clienteInteressado, $Default);


            /*
             * Salva os telefones do cliente
             */
            $arrTelefone = array();

            $Tbclienteinteressadotelefone = new Tbclienteinteressadotelefone();
            $Tbclienteinteressadotelefone->removeAll("id_clienteinteressado = '{$id}'", $Default);
            if (!empty($clienteInteressadoTelefone)) {
                foreach ($clienteInteressadoTelefone['tx_telefones'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_telefone'] = $telefone;
                }
                foreach ($clienteInteressadoTelefone['tx_tipo'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_tipo'] = $telefone;
                }

                foreach ($arrTelefone as $telefone) {
                    $telefone['id_clienteinteressado'] = $id;
                    $Tbclienteinteressadotelefone->save($telefone, $Default);
                }
            }


            /*
             * Salva os e-mails do cliente
             */
            $Tbclienteinteressadoemail = new Tbclienteinteressadoemail();

            $Tbclienteinteressadoemail->removeAll("id_clienteinteressado = '{$id}'", $Default);
            if (!empty($clienteInteressadoEmail)) {
                $emailSave = array();
                foreach ($clienteInteressadoEmail['tx_email'] as $email) {
                    $emailSave['tx_email'] = $email;
                    $emailSave['id_clienteinteressado'] = $id;

                    $Tbclienteinteressadoemail->save($emailSave, $Default);
                }
            }

            $arrEndereco = array();
            if (!empty($clienteInteressadoEndereco)) {
                foreach ($clienteInteressadoEndereco['tx_cep'] as $key => $tx_cep) {
                    $arrEndereco[$key]['tx_cep'] = $tx_cep;
                }
                foreach ($clienteInteressadoEndereco['tx_uf'] as $key => $tx_uf) {
                    $arrEndereco[$key]['tx_uf'] = $tx_uf;
                }
                foreach ($clienteInteressadoEndereco['tx_endereco'] as $key => $tx_endereco) {
                    $arrEndereco[$key]['tx_endereco'] = $tx_endereco;
                }
                foreach ($clienteInteressadoEndereco['tx_numero'] as $key => $tx_numero) {
                    $arrEndereco[$key]['tx_numero'] = $tx_numero;
                }
                foreach ($clienteInteressadoEndereco['tx_cidade'] as $key => $tx_cidade) {
                    $arrEndereco[$key]['tx_cidade'] = $tx_cidade;
                }
                foreach ($clienteInteressadoEndereco['tx_bairro'] as $key => $tx_bairro) {
                    $arrEndereco[$key]['tx_bairro'] = $tx_bairro;
                }
                foreach ($clienteInteressadoEndereco['tx_complemento'] as $key => $tx_complemento) {
                    $arrEndereco[$key]['tx_complemento'] = $tx_complemento;
                }
                foreach ($clienteInteressadoEndereco['tx_observacao'] as $key => $tx_observacao) {
                    $arrEndereco[$key]['tx_observacao'] = $tx_observacao;
                }
            }

            $Tbclienteinteressadoendereco = new Tbclienteinteressadoendereco;
            $Tbclienteinteressadoendereco->removeAll("id_clienteinteressado = '{$id}'", $Default);
            if (!empty($arrEndereco)) {
                foreach ($arrEndereco as $endereco) {
                    $endereco['id_clienteinteressado'] = $id;
                    $Tbclienteinteressadoendereco->save($endereco, $Default);
                }
            }

            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerClienteInteressadoAction() {
        $Default = (new Connection)->getConnection();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');


            $Default->getPdo()->beginTransaction();

            $whereDelete = array("id_clienteinteressado" => $id);

            $Tbclienteinteressadoemail = new \Base\Service\Tbclienteinteressadoemail();
            $Tbclienteinteressadotelefone = new \Base\Service\Tbclienteinteressadotelefone();
            $Tbclienteinteressadoendereco = new \Base\Service\Tbclienteinteressadoendereco();
            $Tbclienteinteressado = new Tbclienteinteressado();

            $Tbclienteinteressadoemail->removeAll($whereDelete, $Default);
            $Tbclienteinteressadotelefone->removeAll($whereDelete, $Default);
            $Tbclienteinteressadoendereco->removeAll($whereDelete, $Default);

            $Tbclienteinteressado->removeAll($whereDelete, $Default);

            $Default->getPdo()->commit();
            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function addInteracaoAction() {
        $params = $this->params()->fromPost();
        $Default = (new Connection)->getConnection();

        $id = (int) $this->params()->fromQuery('id_clienteinteressado');
        $id_cliente = (int) $this->params()->fromQuery('id_cliente');

        $Default->getPdo()->beginTransaction();
        try {
            /*
             * Se estou adicionando uma interação pelo cadastro do cliente
             * pode ser que não haja um CRM cadastrado.
             * Então cadastro este cliente como CRM e depois atribuo a interação
             * ao cliente
             */
            if (empty($id) && !empty($id_cliente) || !empty($id_cliente)) {

                $Tbclienteinteressado = new Tbclienteinteressado();

                $Tbcliente = new Tbcliente;
                $cliente = $Tbcliente->findOneById($id_cliente, ['tx_nome', 'st_tipopessoa', 'id_clienteinteressado']);

                if (empty($cliente['id_clienteinteressado'])) {

                    /*
                     * Salva o cliente como CRM
                     */
                    $id = $Tbclienteinteressado->save([
                        'tx_nome' => $cliente['tx_nome'],
                        'st_tipopessoa' => $cliente['st_tipopessoa'],
                        'id_cliente' => $id_cliente,
                        'st_origemcliente' => 'S'
                            ], $Default
                    );

                    $Tbcliente->save([
                        'id_cliente' => $id_cliente,
                        'id_clienteinteressado' => $id
                            ], $Default);
                } else {
                    $id = $cliente['id_clienteinteressado'];
                }
            }


            if (!empty($id)) {
                $crm = $params['crm'];
                if (empty($crm['tx_interacao'])) {
                    throw new Exception("Adicione a interação que será/foi executada com o cliente interessado.");
                } else if (empty($crm['tx_tipo'])) {
                    throw new Exception("Informe o tipo de interação a realizar/realizada com o cliente interessado.");
                } else if (empty($crm['dt_datahora'])) {
                    throw new Exception("Informe a data e hora da interação executada com o cliente");
                }
            } else {
                throw new Exception("Cliente interessado não identificado. Entre em contato com o suporte para verificar o erro ocorrido.");
            }

            $crm['id_clienteinteressado'] = $id;
            unset($crm['id']);
            $crm['id_usuariotenant_criadopor'] = (new Tbusuariotenant)->getIdUsuarioLogado();
            $dataSave = $crm;

            if (!empty($dataSave['dt_datahora'])) {
                $dataSave['dt_datahora'] = (new BaseController())->reverseDate($dataSave['dt_datahora']);
            }

            $id_interacaoclienteinteressado = (new Tbinteracaoclienteinteressado())->save($dataSave, $Default);


            $Tbinteracaoclienteinteressadoanexo = new Tbinteracaoclienteinteressadoanexo();
            $Tbinteracaoclienteinteressadoanexo->removeAll("id_interacaoclienteinteressado = '{$id_interacaoclienteinteressado}'", $Default);

            if (!empty($params['crm-anexos'])) {
                $arrSaveAnexo = array();

                foreach ($params['crm-anexos'] as $folder => $files) {
                    foreach ($files as $filename) {
                        $arrSaveAnexo['tx_folder'] = $folder;
                        $arrSaveAnexo['tx_nomeservidor'] = $filename;
                        $arrSaveAnexo['tx_nomearquivo'] = $params['crm-anexos-nome-arquivos'][$filename];
                        $arrSaveAnexo['id_interacaoclienteinteressado'] = $id_interacaoclienteinteressado;
                        $Tbinteracaoclienteinteressadoanexo->save($arrSaveAnexo, $Default);
                    }
                }
            }


            $json['type'] = 'success';
            $json['message'] = "Interação adicionada com sucesso.";

            $Default->getPdo()->commit();
        } catch (Exception $ex) {
            $Default->getPdo()->rollBack();

            $json['type'] = 'error';
            $json['message'] = $ex->getMessage();
        }

        echo json_encode($json);
        die;
    }

    public function timelineAction() {
        $id_clienteinteressado = (int) $this->params()->fromQuery('id_clienteinteressado');
        $id_cliente = (int) $this->params()->fromQuery('id_cliente');
        $view = new ViewModel([
            'id_clienteinteressado' => $id_clienteinteressado,
            'id_cliente' => $id_cliente
        ]);
        $view->setTerminal(true);
        return $view;
    }

    public function excluirInteracaoCrmAction() {

        $id_interacaocrm = $this->params()->fromQuery('id');

        $Default = (new Connection())->getConnection();

        $Default->getPdo()->beginTransaction();
        try {
            (new Tbinteracaoclienteinteressadoanexo())->removeAll("id_interacaoclienteinteressado = '{$id_interacaocrm}'", $Default);
            (new Tbinteracaoclienteinteressado())->removeAll("id_interacaoclienteinteressado = '{$id_interacaocrm}'", $Default);

            $json = $this->DeleteCommomReturn();
            $Default->getPdo()->commit();
        } catch (Exception $ex) {
            $Default->getPdo()->rollBack();
            $json = $this->ExceptionCommonReturn($ex->getMessage());
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function getInteracaoCrmAction() {

        $id_interacaocrm = $this->params()->fromQuery('id');

        $Default = (new Connection())->getConnection();

        $Default->getPdo()->beginTransaction();
        try {
            $interacao = (new Tbinteracaoclienteinteressado())->findOneById($id_interacaocrm);

            if (!empty($interacao['dt_datahora'])) {
                $interacao['dt_datahora'] = (new BaseController)->reverseDate($interacao['dt_datahora']);
            }

            $anexos = (new Tbinteracaoclienteinteressadoanexo())->fetchAll(array(
                'id_interacaoclienteinteressado' => $id_interacaocrm
            ));
            if (!empty($anexos)) {
                foreach ($anexos as $key => $anexo) {
                    $arrAnexos['folder'] = $anexo['tx_folder'];
                    $arrAnexos['arquivos'][$key]['nomeServidor'] = $anexo['tx_nomeservidor'];
                    $arrAnexos['arquivos'][$key]['nomeArquivo'] = $anexo['tx_nomearquivo'];
                }
                $json['anexos'] = $arrAnexos;
            }

            $json['interacao'] = $interacao;
            $Default->getPdo()->commit();
        } catch (Exception $ex) {
            $Default->getPdo()->rollBack();
            $json = $this->ExceptionCommonReturn($ex->getMessage());
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function uploadAnexoAction() {
        $anexos = $this->params()->fromFiles();

        try {
            if (!empty($anexos)) {
                $Upload = new UploadController();

                $id_tenant = (new Tbtenant())->getIdTenant();
                $folder = getcwd() . '/public/uploads/crm-interacao/anexos/' . md5($id_tenant);
                if (!is_dir($folder)) {
                    mkdir($folder, '0777', true);
                }

                $arquivosUpload = $Upload->uploadFile($anexos['anexos'], $folder);

                $json["folder"] = "uploads/crm-interacao/anexos/" . md5($id_tenant);
                $json['arquivos'] = $arquivosUpload;
            } else {
                $json['message'] = "Upload não foi realizado";
            }
        } catch (Exception $ex) {
            $json['message'] = $this->ExceptionCommonReturn($ex->getMessage());
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function criarClienteBaseadoInteressadoAction() {
        $id = (int) $this->params()->fromQuery('id');

        $Default = (new Connection())->getConnection();
        $Default->getPdo()->beginTransaction();
        try {
            if (empty($id)) {
                throw new Exception("Cadastro de Cliente Interessado não foi encontrado. Favor entrar em contato com o suporte do sistema.");
            }

//INFORMAÇÕES CLIENTE INTERESSADO
            $Tbclienteinteressado = new Tbclienteinteressado();
            $Tbclienteinteressadoemail = new Tbclienteinteressadoemail();
            $Tbclienteinteressadoendereco = new Tbclienteinteressadoendereco();
            $Tbclienteinteressadotelefone = new Tbclienteinteressadotelefone();

            $clienteInteressado = $Tbclienteinteressado->findOneById($id);
            $clienteInteressadoEmail = $Tbclienteinteressadoemail->fetchAll(array('id_clienteinteressado' => $id));
            $clienteInteressadoEndereco = $Tbclienteinteressadoendereco->fetchAll(array('id_clienteinteressado' => $id));
            $clienteInteressadoTelefone = $Tbclienteinteressadotelefone->fetchAll(array('id_clienteinteressado' => $id));


//INFORMAÇÕES CLIENTE
            $Tbcliente = new Tbcliente();
            $Tbclienteemail = new Tbclienteemail();
            $Tbclienteendereco = new Tbclienteendereco();
            $Tbclientetelefone = new Tbclientetelefone();

            unset($clienteInteressado['dt_primeirocontato']);
            unset($clienteInteressado['tx_origemcontato']);
            unset($clienteInteressado['tx_objetivocontato']);
            unset($clienteInteressado['st_origemcliente']);

            $id_cliente = $Tbcliente->save($clienteInteressado, $Default);
            $clienteInteressado['id_cliente'] = $id_cliente;


            $Tbclienteinteressado->save($clienteInteressado, $Default);

            if (!empty($clienteInteressadoEmail)) {
                foreach ($clienteInteressadoEmail as $email) {
                    unset($email['id_clientelinteressadoemail']);
                    unset($email['id_clienteinteressado']);
                    $email['id_cliente'] = $id_cliente;

                    $Tbclienteemail->save($email, $Default);
                }
            }

            if (!empty($clienteInteressadoEndereco)) {
                foreach ($clienteInteressadoEndereco as $endereco) {
                    unset($endereco['id_clienteinteressadoendereco']);
                    unset($endereco['id_clienteinteressado']);
                    $endereco['id_cliente'] = $id_cliente;

                    $Tbclienteendereco->save($endereco, $Default);
                }
            }


            if (!empty($clienteInteressadoTelefone)) {
                foreach ($clienteInteressadoTelefone as $telefone) {
                    unset($telefone['id_clienteinteressadotelefone']);
                    unset($telefone['id_clienteinteressado']);
                    $telefone['id_cliente'] = $id_cliente;

                    $Tbclientetelefone->save($telefone, $Default);
                }
            }


            $Default->getPdo()->commit();

            $json['type'] = 'success';
            $json['message'] = "Cadastro de cliente realizado com sucesso.";
        } catch (Exception $ex) {
            $Default->getPdo()->rollBack();

            $json['type'] = 'error';
            $json['message'] = $this->tratativaMensagem($ex->getMessage());
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function desvincularClienteInteressadoAction() {
        $id = $this->params()->fromQuery('id');
        $apagarCliente = $this->params()->fromQuery('apagarCliente');

        if (!empty($id) && !empty($apagarCliente)) {

            $Default = (new Connection())->getConnection();
            $Default->getPdo()->beginTransaction();
            try {

                $Tbcliente = new Tbcliente();
                $Tbclienteemail = new Tbclienteemail();
                $Tbclienteendereco = new Tbclienteendereco();
                $Tbclientetelefone = new Tbclientetelefone();
                $Tbclienteinteressado = new Tbclienteinteressado();

                $clienteInteresado = ['id_clienteinteressado' => $id, 'id_cliente' => null];
                $Tbclienteinteressado->save($clienteInteresado, $Default);

                $cliente = current($Tbcliente->fetchAll(array('id_clienteinteressado' => $id)));

                $cliente['id_clienteinteressado'] = null;
//                print "<pre>";
//                print_r($cliente);
//                die;
                $Tbcliente->save($cliente, $Default);

                if ($apagarCliente == 'S') {
                    $Tbclientetelefone->removeAll("id_cliente = '{$cliente['id_cliente']}'", $Default);
                    $Tbclienteemail->removeAll("id_cliente = '{$cliente['id_cliente']}'", $Default);
                    $Tbclienteendereco->removeAll("id_cliente = '{$cliente['id_cliente']}'", $Default);
                    $Tbcliente->removeAll("id_cliente = '{$cliente['id_cliente']}'", $Default);
                }

                $json['type'] = 'success';
                $json['message'] = "Cliente desvínculado com sucesso.";
                $Default->getPdo()->commit();
            } catch (Exception $ex) {
                $Default->getPdo()->rollBack();

                $json['type'] = 'error';
                $json['message'] = $this->tratativaMensagem($ex->getMessage());
            }
        } else {
            $json['type'] = 'error';
            $json['message'] = $this->tratativaMensagem("Algo ocorreu errado, favor entrar em contato com o suporte do sistema.");
        }


        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
