<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\CustomActionController;
use Zend\View\Model\ViewModel;

class InicioController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = 'inicio';
    }

    public function indexAction() {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    public function sairAction() {
        try {
            $adminContainer = new \Zend\Session\Container('admin');
            $adminContainer->getDefaultManager()->destroy();
            $usuarioLogadoContainer = new \Zend\Session\Container('usuarioLogado');
            $usuarioLogadoContainer->getDefaultManager()->destroy();
        } catch (Exception $ex) {
            error_log("erro ao sair do sistema: " . $ex->getMessage());
        }
        return $this->redirect()->toUrl('/admin/index');
    }

}
