<?php

//commit

namespace Admin\Controller;

use Base\Controller\BaseController;
use Base\Service\Connection;
use Base\Service\Tbfuncionario;
use Base\Service\Tbfuncionarioafastamento;
use Base\Service\Tbfuncionariodependente;
use Base\Service\Tbfuncionarioemail;
use Base\Service\Tbfuncionarioendereco;
use Base\Service\Tbfuncionarioferias;
use Base\Service\Tbfuncionariotelefone;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class FuncionarioController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "funcionario";
        $this->tituloPagina = "Funcionários";
        $this->subTituloPagina = "Cadastro de Funcionários";
    }

    public function indexAction() {
        $funcionarios = (new Tbfuncionario())->fetchAll();

        $viewModel = new ViewModel(array(
            'funcionarios' => $funcionarios
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_funcionario = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_funcionario)) {
            $Tbfuncionario = new Tbfuncionario();

            $dataPopulate = $Tbfuncionario->findOneById($id_funcionario);

            if (!empty($dataPopulate)) {
                if (!empty($dataPopulate['dt_nascimento'])) {
                    $dataPopulate['dt_nascimento'] = (new BaseController())->reverseDate($dataPopulate['dt_nascimento']);
                }
                if (!empty($dataPopulate['vl_salario'])) {
                    $dataPopulate['vl_salario'] = (new BaseController())->floatToMoney($dataPopulate['vl_salario']);
                }
                $arrPopulate['funcionario'] = $dataPopulate;
            } else {
                return $this->redirect()->toUrl('/admin/funcionario/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }

        $Tbfuncionariodependente = new Tbfuncionariodependente();
        $dependentes = $Tbfuncionariodependente->fetchAll(['id_funcionario' => $id_funcionario]);

        $Tbfuncionarioferias = new Tbfuncionarioferias();
        $ferias = $Tbfuncionarioferias->fetchAll(['id_funcionario' => $id_funcionario]);

        $Tbfuncionarioafastamento = new Tbfuncionarioafastamento();
        $afastamentos = $Tbfuncionarioafastamento->fetchAll(['id_funcionario' => $id_funcionario]);

        return new ViewModel(array(
            'dependentes' => json_encode($dependentes),
            'id_funcionario' => $id_funcionario,
            'ferias' => json_encode($ferias),
            'afastamentos' => json_encode($afastamentos),
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }
            $funcionario = $this->request->getPost('funcionario');
            $funcionarioTelefone = $this->request->getPost('funcionario-telefone');
            $funcionarioEmail = $this->request->getPost('funcionario-email');
            $funcionarioEndereco = $this->request->getPost('funcionario-endereco');
            $dependentes = $this->request->getPost('dependentes');
            $ferias = $this->request->getPost('ferias');
            $afastamentos = $this->request->getPost('afastamentos');
            $salario = $this->request->getPost('salario');

            $Tbfuncionario = new Tbfuncionario();

            if (!empty($funcionario['id_funcionario'])) {
                $funcionario['id_funcionario'] = (int) $funcionario['id_funcionario'];
            }

            if (empty($funcionario['id_tenant'])) {
                $funcionario['id_tenant'] = (new Container('admin'))->id_tenant;
            }

            if (!empty($funcionario['dt_nascimento'])) {
                $funcionario['dt_nascimento'] = (new BaseController())->reverseDate($funcionario['dt_nascimento']);
            }

            if (!empty($funcionario['vl_salario'])) {
                $funcionario['vl_salario'] = (new BaseController())->moneyToFloat($funcionario['vl_salario']);
            }

            /*
             * Salva o o funcionario
             */
            $id = $Tbfuncionario->save($funcionario, $Default);


            $Tbfuncionariotelefone = new Tbfuncionariotelefone();
            $Tbfuncionariotelefone->removeAll("id_funcionario = '{$id}'", $Default);
            /*
             * Salva os telefones do funcionario
             */
            $arrTelefone = array();
            if (!empty($funcionarioTelefone)) {
                foreach ($funcionarioTelefone['tx_telefones'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_telefone'] = $telefone;
                }
                foreach ($funcionarioTelefone['tx_tipo'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_tipo'] = $telefone;
                }

                foreach ($arrTelefone as $telefone) {
                    $telefone['id_funcionario'] = $id;
                    $Tbfuncionariotelefone->save($telefone, $Default);
                }
            }


            $Tbfuncionarioemail = new Tbfuncionarioemail();
            $Tbfuncionarioemail->removeAll("id_funcionario = '{$id}'", $Default);
            if (!empty($funcionarioEmail)) {
                /*
                 * Salva os e-mails do funcionario
                 */
                $emailSave = array();
                foreach ($funcionarioEmail['tx_email'] as $email) {
                    $emailSave['tx_email'] = $email;
                    $emailSave['id_funcionario'] = $id;

                    $Tbfuncionarioemail->save($emailSave, $Default);
                }
            }

            $arrEndereco = array();
            if (!empty($funcionarioEndereco)) {
                foreach ($funcionarioEndereco['tx_cep'] as $key => $tx_cep) {
                    $arrEndereco[$key]['tx_cep'] = $tx_cep;
                }
                foreach ($funcionarioEndereco['tx_uf'] as $key => $tx_uf) {
                    $arrEndereco[$key]['tx_uf'] = $tx_uf;
                }
                foreach ($funcionarioEndereco['tx_endereco'] as $key => $tx_endereco) {
                    $arrEndereco[$key]['tx_endereco'] = $tx_endereco;
                }
                foreach ($funcionarioEndereco['tx_numero'] as $key => $tx_numero) {
                    $arrEndereco[$key]['tx_numero'] = $tx_numero;
                }
                foreach ($funcionarioEndereco['tx_cidade'] as $key => $tx_cidade) {
                    $arrEndereco[$key]['tx_cidade'] = $tx_cidade;
                }
                foreach ($funcionarioEndereco['tx_bairro'] as $key => $tx_bairro) {
                    $arrEndereco[$key]['tx_bairro'] = $tx_bairro;
                }
                foreach ($funcionarioEndereco['tx_complemento'] as $key => $tx_complemento) {
                    $arrEndereco[$key]['tx_complemento'] = $tx_complemento;
                }
                foreach ($funcionarioEndereco['tx_observacao'] as $key => $tx_observacao) {
                    $arrEndereco[$key]['tx_observacao'] = $tx_observacao;
                }
            }

            $Tbfuncionarioendereco = new Tbfuncionarioendereco;
            $Tbfuncionarioendereco->removeAll("id_funcionario = '{$id}'", $Default);
            if (!empty($arrEndereco)) {
                foreach ($arrEndereco as $endereco) {
                    $endereco['id_funcionario'] = $id;
                    $Tbfuncionarioendereco->save($endereco, $Default);
                }
            }


            if (!empty($dependentes)) {
                $Tbfuncionariodependentes = new Tbfuncionariodependente();
                $Tbfuncionariodependentes->removeAll("id_funcionario = '{$id}'", $Default);

                foreach ($dependentes['tx_nome'] as $key => $nome) {
                    $saveDependente = [
                        'tx_nome' => $nome,
                        'tx_parentesco' => $dependentes['tx_parentesco'][$key],
                        'dt_nascimento' => (new BaseController())->reverseDate($dependentes['dt_nascimento'][$key]),
                        'id_funcionario' => $id
                    ];

                    $Tbfuncionariodependentes->save($saveDependente, $Default);
                }
            }


            if (!empty($ferias)) {
                $Tbfuncionarioferias = new Tbfuncionarioferias();
                $Tbfuncionarioferias->removeAll("id_funcionario = '{$id}'", $Default);

                foreach ($ferias['dt_inicio'] as $key => $dt_inicio) {
                    $saveFerias = [
                        'dt_inicio' => (new BaseController())->reverseDate($ferias['dt_inicio'][$key]),
                        'dt_fim' => (new BaseController())->reverseDate($ferias['dt_fim'][$key]),
                        'id_funcionario' => $id
                    ];

                    $Tbfuncionarioferias->save($saveFerias, $Default);
                }
            }

            if (!empty($afastamentos)) {
                $Tbfuncionarioafastamento = new Tbfuncionarioafastamento();
                $Tbfuncionarioafastamento->removeAll("id_funcionario = '{$id}'", $Default);

                foreach ($afastamentos['dt_inicio'] as $key => $dt_inicio) {
                    $saveAfastamento = [
                        'dt_inicio' => (new BaseController())->reverseDate($afastamentos['dt_inicio'][$key]),
                        'dt_fim' => (new BaseController())->reverseDate($afastamentos['dt_fim'][$key]),
                        'tx_motivo' => $afastamentos['tx_motivo'][$key],
                        'id_funcionario' => $id
                    ];

                    $Tbfuncionarioafastamento->save($saveAfastamento, $Default);
                }
            }


            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id;
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerFuncionarioAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');



            $whereDelete = array("id_funcionario" => $id);

            $Tbfuncionarioemail = new Tbfuncionarioemail();
            $Tbfuncionariotelefone = new Tbfuncionariotelefone();
            $Tbfuncionarioendereco = new Tbfuncionarioendereco();
            $Tbfuncionario = new Tbfuncionario();

            $Tbfuncionarioemail->removeAll($whereDelete, $Default);
            $Tbfuncionariotelefone->removeAll($whereDelete, $Default);
            $Tbfuncionarioendereco->removeAll($whereDelete, $Default);
            $Tbfuncionario->removeAll($whereDelete, $Default);

            $Default->getPdo()->commit();
            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
