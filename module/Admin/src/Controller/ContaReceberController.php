<?php

namespace Admin\Controller;

use Base\Controller\BaseController;
use Base\Controller\UploadController;
use Base\Service\Connection;
use Base\Service\Tbcategoriaconta;
use Base\Service\Tbcontareceber;
use Base\Service\Tbcontareceberanexo;
use Base\Service\Tbtenant;
use DateTime;
use Exception;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\View\Model\ViewModel;

class ContaReceberController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "conta-receber";
        $this->tituloPagina = "Contas a Receber";
        $this->subTituloPagina = "Cadastro de Contas a Receber";
    }

    public function indexAction() {
        $contas = (new Tbcontareceber())->fetchAll(['order' => 'id_contareceber DESC']);

        $viewModel = new ViewModel(array(
            'contaReceber' => $contas,
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_contareceber = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_contareceber)) {
            $Tbcontareceber = new Tbcontareceber();

            $dataPopulate = $Tbcontareceber->findOneById($id_contareceber);

            if (!empty($dataPopulate)) {
                if (!empty($dataPopulate['st_tiporepetir'])) {
                    $dataPopulate['st_repetir'] = 'on';
                }

                if (!empty($dataPopulate['dt_competencia'])) {
                    $dataPopulate['dt_competencia'] = (new BaseController())->reverseDate($dataPopulate['dt_competencia']);
                }

                if (!empty($dataPopulate['dt_vencimento'])) {
                    $dataPopulate['dt_vencimento'] = (new BaseController())->reverseDate($dataPopulate['dt_vencimento']);
                }

                if (!empty($dataPopulate['vl_total'])) {
                    $dataPopulate['vl_total'] = (new BaseController())->floatToMoney($dataPopulate['vl_total']);
                }

                if (!empty($dataPopulate['vl_multa'])) {
                    $casas = 2;
                    if ($dataPopulate['st_tipomulta'] == 'P') {
                        $casas = 4;
                    }
                    $dataPopulate['vl_multa'] = (new BaseController())->floatToMoney($dataPopulate['vl_multa'], $casas);
                }
                if (!empty($dataPopulate['vl_mora'])) {
                    $casas = 2;
                    if ($dataPopulate['st_tipomora'] == 'P') {
                        $casas = 4;
                    }
                    $dataPopulate['vl_mora'] = (new BaseController())->floatToMoney($dataPopulate['vl_mora'], $casas);
                }

                if (!empty($dataPopulate['id_categoriaconta'])) {
                    $categoria = (new Tbcategoriaconta())->findOneById($dataPopulate['id_categoriaconta']);
                    $dataPopulate['tx_categoria'] = $categoria['tx_categoria'];
                }

                $arrPopulate['conta-receber'] = $dataPopulate;
            } else {
                $this->redirect()->toUrl('/admin/conta-receber/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }

        return new ViewModel(array(
            'id_contareceber' => $id_contareceber
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $Tbcontareceber = new Tbcontareceber();

            $conta = $this->request->getPost('conta-receber');
            $conta_receber_arquivos = $this->request->getPost('conta-receber-arquivo');
            $conta_receber_nome_arquivos = $this->request->getPost('conta-receber-nome-arquivos');


            if (!empty($conta['dt_competencia'])) {
                $conta['dt_competencia'] = (new BaseController)->reverseDate($conta['dt_competencia']);
            }

            if (!empty($conta['dt_vencimento'])) {
                $conta['dt_vencimento'] = (new BaseController)->reverseDate($conta['dt_vencimento']);
            }

            if (!empty($conta['vl_total'])) {
                $conta['vl_total'] = (new BaseController)->moneyToFloat($conta['vl_total']);
            }

            if (!empty($conta['vl_multa'])) {
                $conta['vl_multa'] = (new BaseController)->moneyToFloat($conta['vl_multa']);
            }

            if (!empty($conta['vl_mora'])) {
                $conta['vl_mora'] = (new BaseController)->moneyToFloat($conta['vl_mora']);
            }

            if (!empty($conta['st_repetir'])) {
                $st_repetir = $conta['st_repetir'];
                unset($conta['st_repetir']);
            } else {
                $st_repetir = '';
            }


            $id_categoriaconta = (new Tbcategoriaconta())->verificaInsereCategoria($conta['tx_categoria']);
            $conta['id_categoriaconta'] = $id_categoriaconta;
            unset($conta['tx_categoria']);

            $id_contareceber = $Tbcontareceber->save($conta, $Default);

            if (!empty($st_repetir)) {
                if ($conta['st_tiporepetir'] == 'diaria') {
                    $repeticao = "+1 day";
                } else if ($conta['st_tiporepetir'] == 'semana') {
                    $repeticao = "+1 week";
                } else if ($conta['st_tiporepetir'] == 'mes') {
                    $repeticao = "+1 month";
                } else if ($conta['st_tiporepetir'] == 'bimestre') {
                    $repeticao = "+2 months";
                } else if ($conta['st_tiporepetir'] == 'trimestre') {
                    $repeticao = "+3 months";
                } else if ($conta['st_tiporepetir'] == 'semestre') {
                    $repeticao = "+6 months";
                } else if ($conta['st_tiporepetir'] == 'anual') {
                    $repeticao = "+1 year";
                }

                $dt_vencimento_conta_nova = null;
                for ($i = 1; $i <= $conta['nr_ocorrencias']; $i++) {
                    if (empty($dt_vencimento_conta_nova)) {
                        $Date1 = $conta['dt_vencimento'];
                    } else {
                        $Date1 = $dt_vencimento_conta_nova;
                    }
                    $date = new DateTime($Date1);
                    $date->modify($repeticao);

                    $dt_vencimento_conta_nova = $date->format('Y-m-d');

                    if (!empty($conta['st_lancarcomoparcelamento'])) {
                        $novaConta['tx_descricao'] = "({$i}/{$conta['nr_ocorrencias']}) " . $conta['tx_descricao'];
                    } else {
                        $novaConta['tx_descricao'] = $conta['tx_descricao'];
                    }

                    $novaConta['id_categoriaconta'] = $id_categoriaconta;
                    $novaConta['id_bancoconta'] = $conta['id_bancoconta'];
                    $novaConta['dt_competencia'] = $conta['dt_competencia'];
                    $novaConta['dt_vencimento'] = $dt_vencimento_conta_nova;
                    $novaConta['vl_total'] = $conta['vl_total'];
                    $novaConta['st_tipomulta'] = $conta['st_tipomulta'];
                    $novaConta['vl_multa'] = $conta['vl_multa'];
                    $novaConta['st_tipomora'] = $conta['st_tipomora'];
                    $novaConta['vl_mora'] = $conta['vl_mora'];
                    $novaConta['st_tiporepetir'] = $conta['st_tiporepetir'];
                    $novaConta['nr_ocorrencias'] = $conta['nr_ocorrencias'];
                    $novaConta['tx_observacao'] = $conta['tx_observacao'];
                    $novaConta['id_contareceberorigem'] = $id_contareceber;

                    $Tbcontareceber->save($novaConta, $Default);
                }
            }



            $Tbcontareceberanexo = new Tbcontareceberanexo();
            $Tbcontareceberanexo->removeAll("id_contareceber = '{$id_contareceber}'", $Default);

            if (!empty($conta_receber_arquivos)) {
                $arrSaveAnexo = array();

                foreach ($conta_receber_arquivos as $folder => $files) {
                    foreach ($files as $filename) {
                        $arrSaveAnexo['tx_folder'] = $folder;
                        $arrSaveAnexo['tx_nomeservidor'] = $filename;
                        $arrSaveAnexo['tx_nomearquivo'] = $conta_receber_nome_arquivos[$filename];
                        $arrSaveAnexo['id_contareceber'] = $id_contareceber;
                        $Tbcontareceberanexo->save($arrSaveAnexo, $Default);
                    }
                }
            }

            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id_contareceber;
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerContasAction() {
        $Default = (new Connection)->getConnection();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');


            $Default->getPdo()->beginTransaction();

            $Tbcontareceber = new Tbcontareceber();
            $Tbcontareceberpermissao = new \Base\Service\Tbcontareceberpermissao();
            $Tbcontareceberpermissao->removeAll(array('id_contareceber' => $id), $Default);
            $Tbcontareceber->removeAll(array('id_contareceber' => $id), $Default);

            $Default->getPdo()->commit();

            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function uploadAnexoAction() {
        $anexos = $this->params()->fromFiles();
        try {
            if (!empty($anexos)) {
                $Upload = new UploadController();

                $id_tenant = (new Tbtenant())->getIdTenant();
                $folder = getcwd() . '/public/uploads/conta-receber/anexos/' . md5($id_tenant);
                if (!is_dir($folder)) {
                    mkdir($folder, '0777', true);
                }
                $arquivosUpload = $Upload->uploadFile($anexos['anexos'], $folder);

                $json["folder"] = "uploads/conta-receber/anexos/" . md5($id_tenant);
                $json['arquivos'] = $arquivosUpload;
            } else {
                $json['message'] = "Upload não foi realizado";
            }
        } catch (Exception $ex) {
            $json['message'] = $this->ExceptionCommonReturn($ex->getMessage());
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
