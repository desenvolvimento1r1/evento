<?php

namespace Admin\Controller;

use Base\Service\Connection;
use Base\Service\Tbusuariotenant;
use Exception;
use PDOException;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class UsuarioController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "usuario";
        $this->tituloPagina = "Usuários";
        $this->subTituloPagina = "Cadastro de Usuários";
    }

    public function indexAction() {
        $usuarios = (new Tbusuariotenant())->fetchAll();

        $viewModel = new ViewModel(array(
            'usuarios' => $usuarios
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_usuariotenant = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_usuariotenant)) {
            $Tbusuariotenant = new Tbusuariotenant();

            $dataPopulate = $Tbusuariotenant->findOneById($id_usuariotenant);

            if (!empty($dataPopulate)) {
                if ($dataPopulate['st_ativo'] == 1) {
                    $dataPopulate['st_ativo'] = 'on';
                }
                $arrPopulate['usuario'] = $dataPopulate;

                unset($arrPopulate['usuario']['tx_senha']);
            } else {
                $this->redirect()->toUrl('/admin/usuario/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }

        $Tbpermissao = new \Base\Service\Tbpermissao();
        $paineisPermissoes = $Tbpermissao->fetchAll(array('group_by' => 'tx_panel'));

        if (!empty($id_usuariotenant)) {
            $Tbusuariotenantpermissao = new \Base\Service\Tbusuariotenantpermissao();
            $permissoesUsuario = $Tbusuariotenantpermissao->fetchAll(array('id_usuariotenant' => $id_usuariotenant, 'columns' => array('p.id_permissao')));

            $permissoesUsuario = array_column($permissoesUsuario, 'id_permissao');
        } else {
            $permissoesUsuario = array();
        }
        return new ViewModel(array(
            'paineisPermissoes' => $paineisPermissoes,
            'permissoesUsuario' => $permissoesUsuario,
            'id_usuariotenant' => $id_usuariotenant
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }
            $usuario = $this->request->getPost('usuario');
            $permissoes = $this->request->getPost('permissoes');

            $Tbusuariotenant = new Tbusuariotenant();


            if (!empty($usuario['id_usuariotenant'])) {
                $usuario['id_usuariotenant'] = (int) $usuario['id_usuariotenant'];
            }

            if (empty($usuario['st_ativo'])) {
                $usuario['st_ativo'] = 'N';
            }

            if (empty($usuario['id_tenant'])) {
                $usuario['id_tenant'] = (new Container('admin'))->id_tenant;
            }

            if (!empty($usuario['tx_senha'])) {
                $usuario['tx_senha'] = sha1($usuario['tx_senha']);
            }

            $id = $Tbusuariotenant->save($usuario, $Default);

            $Tbusuariotenantpermissao = new \Base\Service\Tbusuariotenantpermissao();

            $Tbusuariotenantpermissao->removeAll("id_usuariotenant = '{$usuario['id_usuariotenant']}'", $Default);
            if (!empty($permissoes)) {
                foreach ($permissoes as $id_permissao => $on) {
                    $dataPermissaoSalvar = array(
                        'id_usuariotenant' => $usuario['id_usuariotenant'],
                        'id_tenant' => $usuario['id_tenant'],
                        'id_permissao' => $id_permissao
                    );

                    $Tbusuariotenantpermissao->save($dataPermissaoSalvar, $Default);
                }
            }

            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id;
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerUsuarioAction() {
        $Default = (new Connection)->getConnection();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');


            $Default->getPdo()->beginTransaction();

            $Tbusuariotenant = new Tbusuariotenant();
            $Tbusuariotenantpermissao = new \Base\Service\Tbusuariotenantpermissao();
            $Tbusuariotenantpermissao->removeAll(array('id_usuariotenant' => $id), $Default);
            $Tbusuariotenant->removeAll(array('id_usuariotenant' => $id), $Default);

            $Default->getPdo()->commit();

            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
