<?php

//commit

namespace Admin\Controller;

use Base\Service\Connection;
use Base\Service\Tbcliente;
use Exception;
use PDOException;
use Zend\Json\Json;
use Zend\Mvc\Controller\CustomActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class ClienteController extends CustomActionController {

    public function __construct() {
        $this->module = "admin";
        $this->controller = "cliente";
        $this->tituloPagina = "Clientes";
        $this->subTituloPagina = "Cadastro de Clientes";
    }

    public function indexAction() {
        $clientes = (new Tbcliente())->fetchAll();

        $viewModel = new ViewModel(array(
            'clientes' => $clientes
        ));
        return $viewModel;
    }

    public function formularioAction() {
        $id_cliente = (int) $this->params()->fromQuery('id');

        $arrPopulate = array();

        if (!empty($id_cliente)) {
            $Tbcliente = new Tbcliente();

            $dataPopulate = $Tbcliente->findOneById($id_cliente);

            if (!empty($dataPopulate)) {
                if (!empty($dataPopulate['dt_nascimento'])) {
                    $dataPopulate['dt_nascimento'] = (new \Base\Controller\BaseController())->reverseDate($dataPopulate['dt_nascimento']);
                }
                $arrPopulate['cliente'] = $dataPopulate;
            } else {
                return $this->redirect()->toUrl('/admin/cliente/index');
            }
        }

        if (!empty($arrPopulate)) {
            $this->layout()->setVariables(array('dataPopulate' => json_encode($arrPopulate)));
        }

        return new ViewModel(array(
            'id_cliente' => $id_cliente,
            'id_clienteinteressado' => $dataPopulate['id_clienteinteressado']
        ));
    }

    public function salvarAction() {
        $Default = (new Connection)->getConnection();
        $Default->getPdo()->beginTransaction();

        try {
            if (!$this->possuiPermissaoCadastrar) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }
            $cliente = $this->request->getPost('cliente');
            $clienteTelefone = $this->request->getPost('cliente-telefone');
            $clienteEmail = $this->request->getPost('cliente-email');
            $clienteEndereco = $this->request->getPost('cliente-endereco');

            $Tbcliente = new Tbcliente();

            if (!empty($cliente['id_cliente'])) {
                $cliente['id_cliente'] = (int) $cliente['id_cliente'];
            }

            if (empty($cliente['id_tenant'])) {
                $cliente['id_tenant'] = (new Container('admin'))->id_tenant;
            }

            if (!empty($cliente['dt_nascimento'])) {
                $cliente['dt_nascimento'] = (new \Base\Controller\BaseController())->reverseDate($cliente['dt_nascimento']);
            }
//            print "<pre>";
//            print_r($cliente);
//            die;
            /*
             * Salva o o cliente
             */
            $id = $Tbcliente->save($cliente, $Default);


            $Tbclientetelefone = new \Base\Service\Tbclientetelefone();
            $Tbclientetelefone->removeAll("id_cliente = '{$id}'", $Default);
            /*
             * Salva os telefones do cliente
             */
            $arrTelefone = array();
            if (!empty($clienteTelefone)) {
                foreach ($clienteTelefone['tx_telefones'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_telefone'] = $telefone;
                }
                foreach ($clienteTelefone['tx_tipo'] as $key => $telefone) {
                    $arrTelefone[$key]['tx_tipo'] = $telefone;
                }

                foreach ($arrTelefone as $telefone) {
                    $telefone['id_cliente'] = $id;
                    $Tbclientetelefone->save($telefone, $Default);
                }
            }


            $Tbclienteemail = new \Base\Service\Tbclienteemail();
            $Tbclienteemail->removeAll("id_cliente = '{$id}'", $Default);
            if (!empty($clienteEmail)) {
                /*
                 * Salva os e-mails do cliente
                 */
                $emailSave = array();
                foreach ($clienteEmail['tx_email'] as $email) {
                    $emailSave['tx_email'] = $email;
                    $emailSave['id_cliente'] = $id;

                    $Tbclienteemail->save($emailSave, $Default);
                }
            }

            $arrEndereco = array();
            if (!empty($clienteEndereco)) {
                foreach ($clienteEndereco['tx_cep'] as $key => $tx_cep) {
                    $arrEndereco[$key]['tx_cep'] = $tx_cep;
                }
                foreach ($clienteEndereco['tx_uf'] as $key => $tx_uf) {
                    $arrEndereco[$key]['tx_uf'] = $tx_uf;
                }
                foreach ($clienteEndereco['tx_endereco'] as $key => $tx_endereco) {
                    $arrEndereco[$key]['tx_endereco'] = $tx_endereco;
                }
                foreach ($clienteEndereco['tx_numero'] as $key => $tx_numero) {
                    $arrEndereco[$key]['tx_numero'] = $tx_numero;
                }
                foreach ($clienteEndereco['tx_cidade'] as $key => $tx_cidade) {
                    $arrEndereco[$key]['tx_cidade'] = $tx_cidade;
                }
                foreach ($clienteEndereco['tx_bairro'] as $key => $tx_bairro) {
                    $arrEndereco[$key]['tx_bairro'] = $tx_bairro;
                }
                foreach ($clienteEndereco['tx_complemento'] as $key => $tx_complemento) {
                    $arrEndereco[$key]['tx_complemento'] = $tx_complemento;
                }
                foreach ($clienteEndereco['tx_observacao'] as $key => $tx_observacao) {
                    $arrEndereco[$key]['tx_observacao'] = $tx_observacao;
                }
            }

            $Tbclienteendereco = new \Base\Service\Tbclienteendereco;
            $Tbclienteendereco->removeAll("id_cliente = '{$id}'", $Default);
            if (!empty($arrEndereco)) {
                foreach ($arrEndereco as $endereco) {
                    $endereco['id_cliente'] = $id;
                    $Tbclienteendereco->save($endereco, $Default);
                }
            }

            $Default->getPdo()->commit();
            $json = $this->SalvarCommomReturn();
            $json['id'] = $id;
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

    public function removerClienteAction() {
        $Default = (new Connection)->getConnection();

        try {
            if (!$this->possuiPermissaoExcluir) {
                throw new Exception("Você não possui permissão para realizar esta ação.");
            }

            $id = (int) $this->params()->fromQuery('id');


            $Default->getPdo()->beginTransaction();

            $whereDelete = array("id_cliente" => $id);

            $Tbclienteemail = new \Base\Service\Tbclienteemail();
            $Tbclientetelefone = new \Base\Service\Tbclientetelefone();
            $Tbclienteendereco = new \Base\Service\Tbclienteendereco();
            $Tbcliente = new Tbcliente();

            $Tbclienteemail->removeAll($whereDelete, $Default);
            $Tbclientetelefone->removeAll($whereDelete, $Default);
            $Tbclienteendereco->removeAll($whereDelete, $Default);
            $Tbcliente->removeAll($whereDelete, $Default);

            $Default->getPdo()->commit();
            $json = $this->DeleteCommomReturn();
        } catch (Exception $ex) {
            $json = $this->ExceptionCommonReturn($ex->getMessage());
            $Default->getPdo()->rollBack();
        }

        $zJson = new Json();
        echo $zJson->encode($json);
        die;
    }

}
