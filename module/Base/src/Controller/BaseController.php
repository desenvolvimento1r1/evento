<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Base\Controller;

use Zend\Mvc\Controller\CustomActionController;

class BaseController extends CustomActionController {

    public function reverseDate($dataParam) {

        $horas = '';
        $dateConverted = $dataParam;

        if (strstr($dataParam, " ")) {
            $dataParam = explode(" ", $dataParam);
            $horas = $dataParam[1];
            $dataParam = $dataParam[0];
        }
        if (strstr($dataParam, '-')) {
            $dateConverted = date('d/m/Y', strtotime($dataParam));
        } else if (strstr($dataParam, '/')) {
            $dateConverted = implode('-', array_reverse(explode('/', ($dataParam))));
        }

        if (!empty($horas)) {
            $dateConverted = $dateConverted . " " . $horas;
        }
        return $dateConverted;
    }

    public function reverseDateToEua($dataParam) {
        if (strstr($dataParam, '-')) {
            return $dataParam;
        } else {
            return $this->reverseDate($dataParam);
        }
    }

    public function getNomMonth($nr_month) {

        $nr_month = (int) $nr_month;

        $months = array();
        $months[1] = 'Janeiro';
        $months[2] = 'Fevereiro';
        $months[3] = 'Março';
        $months[4] = 'Abril';
        $months[5] = 'Maio';
        $months[6] = 'Junho';
        $months[7] = 'Julho';
        $months[8] = 'Agosto';
        $months[9] = 'Setembro';
        $months[10] = 'Outubro';
        $months[11] = 'Novembro';
        $months[12] = 'Dezembro';
        return empty($months[$nr_month]) ? null : $months[$nr_month];
    }

    public function getImgObrigatorio() {
        echo '<span style="color:#961500;font-family:Verdana;font-size:11px;font-weight:700;" title="Campo obrigatório!" class="required">*</span>';
    }

    public function formSelect($options, $value = '') {
        $option = '';

        foreach ($options as $key => $opt) {
            $selected = "";
            if ($key == $value) {
                $selected = 'selected';
            }
            $option .= "<option value='{$key}' {$selected}>{$opt}</option>";
        }
        echo $option;
    }
    
    
    public function moneyToFloat($vl) {
        if (is_float($vl)) {
            return $vl;
        }
        if (is_string($vl) && strpos($vl, ',') !== false) {
            $vl = str_replace('.', '', $vl);
            return str_replace(',', '.', $vl);
        }
        return $vl;
    }

    public function floatToMoney($vl, $decimals = 2) {
        if (empty($vl)) {
            return '0.00';
        }
        if (strpos($vl, ',') !== false) {
            return $vl;
        }
        return number_format($vl, $decimals, ',', '.');
    }


}
