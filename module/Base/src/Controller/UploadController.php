<?php

namespace Base\Controller;

use Zend\Mvc\Controller\CustomActionController;

class UploadController extends CustomActionController {

    public function uploadFile($files, $folder) {
        if (!is_array($files)) {
            $files = array($files);
        }

        $msg = array();
        $errorMsg = array(
            1 => 'O arquivo no upload é maior do que o limite definido em upload_max_filesize no php.ini. ##max_post##',
            2 => 'O arquivo ultrapassa o limite de tamanho em MAX_FILE_SIZE que foi especificado no formulário HTML',
            3 => 'o upload do arquivo foi feito parcialmente',
            4 => 'Não foi feito o upload do arquivo'
        );
        
        foreach ($files as $arquivoUpload) {
            if ($arquivoUpload['error'] != 0) {
                $arquivosEnviados[] = "<b>{$arquivoUpload['name']} :</b> " . $errorMsg[$arquivoUpload['error']];
                continue;
            }

            $extensao = strtolower(@end(explode('.', $arquivoUpload['name'])));
            $novoNome = uniqid("", true) . ".$extensao";

            if (move_uploaded_file($arquivoUpload['tmp_name'], $folder . '/' . $novoNome)) {
                $arquivosEnviados[] = array(
                    'nomeServidor' => $novoNome,
                    'nomeArquivo' => $arquivoUpload['name'],
                );
            } else {
                echo 'não realizou o upload';
                die;
            }
        }

        return $arquivosEnviados;
    }

}
