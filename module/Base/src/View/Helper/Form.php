<?php

namespace Base\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Form extends AbstractHelper {

    public function formSelect($id = null, $class = '', $name, $options, $style = '', $required = '', $function = '', $value = '',$attr='') {
        $select = "<select name='{$name}' id='{$id}' class='{$class}' style='{$style}' {$required} {$function} {$attr}>";
        $option = '';

        foreach ($options as $key => $opt) {
            $selected = "";
            if ($key == $value) {
                $selected = 'selected';
            }
            $option .= "<option value='{$key}' {$selected}>{$opt}</option>";
        }
        echo $select . $option . "</select>";
    }

}
