<?php

// ./module/Application/src/Application/View/Helper/AbsoluteUrl.php

namespace Base\View\Helper;

//use Base\Controller\BuscaCEPController;
use Zend\View\Helper\AbstractHelper;

class AppGlobal extends AbstractHelper {

    public function converteData($dataParam) {
        $base = new \Base\Controller\GlobalController();
        return $base->converteData($dataParam);
    }

    public function floatToMoney($valor, $decimais = 2) {
        $base = new \Base\Controller\GlobalController();
        return $base->floatToMoney($valor, $decimais);
    }

    public function numeroDiasUteis($from, $to) {
        $base = new \Base\Controller\GlobalController();
        return $base->numeroDiasUteis($from, $to);
    }

//    public function buscaCEP($cep) {
//        $buscacep = new BuscaCEPController();
//        return $buscacep->busca($cep);
//    }
//    public function getAlunosPorCategoriaAction($retornaJson = false, $id_categoriaParam = null) {
//        $id_categoria = $id_categoriaParam;
//
//        $Tbcategoriaaluno = $this->getView()->getHelperPluginManager()->getServiceLocator()->get("Base\Service\Tbcategoriaaluno");
//        $categoria_aluno = $Tbcategoriaaluno->getDataAll(array('id_categoriaaluno' => $id_categoria));
//        $idade_maxima = $categoria_aluno[0]['nr_idademaxima'];
//        $idade_minima = $categoria_aluno[0]['nr_idademinima'];
//
//        $dt_maxima = date('Y-m-d', strtotime("-{$idade_maxima} years"));
//        $dt_minimo = date('Y-m-d', strtotime("-{$idade_minima} years"));
////        $dt_maxima = new Ba
//
//        $Tbaluno = $this->getView()->getHelperPluginManager()->getServiceLocator()->get("Base\Service\Tbaluno");
//        $alunos = $Tbaluno->getDataAll(array('dt_maxima' => $dt_maxima, 'dt_minima' => $dt_minimo));
//        foreach ($alunos as $key => $aluno) {
//            $dt_minimo = date('Y-m-d') - $aluno['dt_nascimento'];
//            $alunos[$key]['nr_idadealuno'] = $dt_minimo;
//        }
//        if ($retornaJson) {
//            echo json_encode($alunos);
//            die();
//        } else {
//            return $alunos;
//        }
//    }
}
