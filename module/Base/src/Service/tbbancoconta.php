<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbbancoconta extends CustomAbstractTableGateway {

    protected $table = 'tbbancoconta';
    protected $_primary = 'id_bancoconta';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_bancoconta'])) {
            $query->where("id_bancoconta = '{$params['id_bancoconta']}'");
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['st_ativo'])) {
            $query->where("st_ativo = '{$params['st_ativo']}'");
        }

        return $query->fetchAll();
    }

    public function getArrContas() {
        $contas = $this->fetchAll(array('st_ativo' => 'A'));

        $con = [];
        if (!empty($contas)) {
            foreach ($contas as $conta) {
                $con[$conta['id_bancoconta']] = 'Ag.: ' . $conta['tx_agencia'] . ' / Con.:' . $conta['tx_conta'];
            }
        }

        return $con;
    }

    public function getArrBancoConta($id_arr_banco = '') {
        if (!empty($id_arr_banco)) {
            return $this->_array_bancos[$id_arr_banco];
        }
        return $this->_array_bancos;
    }

    public $_array_bancos = [
        1 => 'BANCO ALFA',
        2 => 'BANCO BANIF',
        3 => 'BANCO BMG',
        4 => 'BANCO BTG PACTUAL',
        5 => 'BANCO CITIBANK',
        6 => 'BANCO COOPERATIVO DO BRASIL',
        7 => 'BANCO COOPERATIVO SICREDI',
        8 => 'BANCO DA AMAZÔNIA',
        9 => 'BANCO DE BRASÍLIA',
        10 => 'BANCO DO BRASIL',
        11 => 'BANCO DO ESTADO DE SERGIPE',
        12 => 'BANCO DO ESTADO DO ESPÍRITO SANTO',
        13 => 'BANCO DO ESTADO DO PARÁ',
        14 => 'BANCO DO ESTADO DO RIO GRANDE DO SUL',
        15 => 'BANCO DO NORDESTE DO BRASIL',
        16 => 'BANCO HSBC BANK BRASIL',
        17 => 'BANCO INTERMEDIUM',
        18 => 'BANCO ITAÚ',
        19 => 'BANCO MARKA',
        20 => 'BANCO MERCANTIL DO BRASIL',
        21 => 'BANCO PANAMERICANO',
        22 => 'BANCO POPULAR DO BRASIL',
        23 => 'BANCO PROSPER',
        24 => 'BANCO RURAL',
        25 => 'BANCO SAFRA',
        26 => 'BANCO SANTANDER',
        27 => 'BANCO SOFISA',
        28 => 'BANCO TOPÁZIO',
        29 => 'BRADESCO',
        30 => 'CAIXA ECONÔMICA FEDERAL',
    ];

}
