<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbfuncionario extends CustomAbstractTableGateway {

    protected $table = 'tbfuncionario';
    protected $_primary = 'id_funcionario';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_funcionario'])) {
            $query->where("id_funcionario = '{$params['id_funcionario']}'");
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        return $query->fetchAll();
    }

    public static function getParentescos($tx_tipo = '') {
        $arrParentesco = [
            "A" => "AVÔ(Ó)",
            "I" => "IRMÃO(Ã)",
            "MA" => "MADRASTA ",
            "M" => "MÃE",
            "O" => "OUTRO",
            "PD" => "PADRASTO",
            "PA" => "PAI",
            "PR" => "PRIMO(A)",
            "T" => "TIO(A)"
        ];

        if (empty($tx_tipo)) {
            return $arrParentesco;
        } else {
            if (isset($arrParentesco[$tx_tipo])) {
                return $arrParentesco[$tx_tipo];
            } else {
                die('parentesco não identificado');
            }
        }
    }

}
