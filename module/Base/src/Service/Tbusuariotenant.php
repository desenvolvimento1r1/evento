<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbusuariotenant extends CustomAbstractTableGateway {

    protected $table = 'tbusuariotenant';
    protected $_primary = 'id_usuariotenant';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_usuariotenant'])) {
            $query->where("id_usuariotenant = '{$params['id_usuariotenant']}'");
        }

        if (!empty($params['usuario'])) {
            $query->where("tx_login LIKE '{$params['usuario']}'");
        }

        if (!empty($params['senha'])) {
            $params['senha'] = sha1($params['senha']);
            $query->where("tx_senha LIKE '{$params['senha']}'");
        }

        if (!empty($params['fb_userid'])) {
            $query->where("fb_userid = '{$params['fb_userid']}'");
        }

        if (!empty($params['st_ativo'])) {
            $query->where("st_ativo = '{$params['st_ativo']}'");
        }

        if (!empty($params['st_admin'])) {
            $query->where("st_admin = '{$params['st_admin']}'");
        }
//echo $query->getQuery();die;

        return $query->fetchAll();
    }

    public function isAdmin($id_usuariotenant = null) {
        if (empty($id_usuariotenant)) {
            $id_usuariotenant = (new Tbusuariotenant())->getIdUsuarioLogado();
        }

        $data = $this->fetchAll(array(
            'id_usuariotenant' => $id_usuariotenant,
            'st_admin' => 'S'
        ));

        if (!empty($data)) {
            return true;
        }
        return false;
    }

    public static function getIdUsuarioLogado() {
        $ContainerUsuarioLogado = new \Zend\Session\Container('usuarioLogado');
        $usuarioLogado = $ContainerUsuarioLogado->getArrayCopy();
        if (empty($usuarioLogado)) {
            return 0;
        }
        return (int) $usuarioLogado['id_usuariotenant'];
    }

    public function getNomeUsuario($id_usuariotenant) {
        $data = $this->findOneById($id_usuariotenant);
        return $data['tx_nomeusuario'];
    }

}
