<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbcontareceberanexo extends CustomAbstractTableGateway {

    protected $table = 'tbcontareceberanexo';
    protected $_primary = 'id_contareceberanexo';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_contareceberanexo'])) {
            $query->where("id_contareceberanexo = '{$params['id_contareceberanexo']}'");
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['id_contareceber'])) {
            $query->where("id_contareceber = '{$params['id_contareceber']}'");
        }

        return $query->fetchAll();
    }

}
