<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbclienteendereco extends CustomAbstractTableGateway {

    protected $table = 'tbclienteendereco';
    protected $_primary = 'id_clienteendereco';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);


        if (!empty($params['id_cliente'])) {
            $query->where("id_cliente = '{$params['id_cliente']}'");
        }
        
        
        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }
        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }


        return $query->fetchAll();
    }

}
