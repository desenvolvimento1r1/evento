<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbinteracaoclienteinteressadoanexo extends CustomAbstractTableGateway {

    protected $table = 'tbinteracaoclienteinteressadoanexo';
    protected $_primary = 'id_interacaoclienteinteressadoanexo';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (!empty($params['id_interacaoclienteinteressado'])) {
            $query->where("id_interacaoclienteinteressado = '{$params['id_interacaoclienteinteressado']}'");
        }

        if (!empty($params['id_interacaoclienteinteressadoanexo'])) {
            $query->where("id_interacaoclienteinteressadoanexo = '{$params['id_interacaoclienteinteressadoanexo']}'");
        }
        
        
        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }
        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }


        if (!empty($params['order'])) {
            $query->orderBy($params['order']);
        }

        return $query->fetchAll();
    }

}
