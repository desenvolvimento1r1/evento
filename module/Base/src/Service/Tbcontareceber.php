<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbcontareceber extends CustomAbstractTableGateway {

    protected $table = 'tbcontareceber';
    protected $_primary = 'id_contareceber';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_contareceber'])) {
            $query->where("id_contareceber = '{$params['id_contareceber']}'");
        }

        if (!empty($params['id_contareceberorigem'])) {
            $query->where("id_contareceberorigem = '{$params['id_contareceberorigem']}'");
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['order'])) {
            $query->orderBy($params['order']);
        }

        return $query->fetchAll();
    }

}
