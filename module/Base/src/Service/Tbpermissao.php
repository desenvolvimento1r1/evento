<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbpermissao extends CustomAbstractTableGateway {

    protected $table = 'tbpermissao';
    protected $_primary = 'id_permissao';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (!empty($params['group_by'])) {
            $query->groupBy(array('tx_panel'));
        }

        if (!empty($params['tx_panel_permissoes'])) {
            $query->where("tx_panel like '{$params['tx_panel_permissoes']}'");
        }

        return $query->fetchAll();
    }

}
