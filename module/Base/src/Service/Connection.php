<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Base\Service;

use FluentPDO;
use PDO;
use Zend\Config\CustomAbstractTableGateway;

/**
 * Description of Connection
 *
 * @author Caique
 */
class Connection extends CustomAbstractTableGateway {

    public function getConnection() {
        $pdo = new PDO("mysql:host=localhost;dbname=evento;charset=utf8", "root", "root");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $fpdo = new FluentPDO($pdo);
        return $fpdo;
    }

}
