<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbinteracaoclienteinteressado extends CustomAbstractTableGateway {

    protected $table = 'tbinteracaoclienteinteressado';
    protected $_primary = 'id_interacaoclienteinteressado';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (!empty($params['id_cliente']) && empty($params['id_clienteinteressado'])) {
            $Tbclienteinteressado = new Tbclienteinteressado();

            $clienteInteressado = $Tbclienteinteressado->fetchAll([
                'id_cliente' => $params['id_cliente'],
                'limit' => '1'
            ]);


            if (!empty($clienteInteressado)) {
                $params['id_clienteinteressado'] = $clienteInteressado[0]['id_clienteinteressado'];
            }
        }

        if (!empty($params['id_clienteinteressado'])) {
            $query->where("id_clienteinteressado = '{$params['id_clienteinteressado']}'");
        } else {
            /*
             * Em teoria, nunca deverá trazer interações de TODOS OS CRMs/clientes
             * por isto foi adicionado este IF.
             * 
             * Caso precise trazer todas as interaçoes de todos os crm/cliente passar o parametro
             * return_all como true
             */
            if (empty($params['return_all'])) {
                return [];
            }
        }

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['order'])) {
            $query->orderBy($params['order']);
        }

        return $query->fetchAll();
    }

    public function getIconTipoInteracao($tx_tipo) {
        $arr = array(
            'ligar' => 'fa fa-phone',
            'reuniao' => 'fa fa-users',
            'tarefa' => 'fa fa-tasks',
            'email' => 'fa fa-at',
            'prazo' => 'fa fa-calendar',
            'almoco' => 'fa fa-cutlery',
        );

        return $arr[$tx_tipo];
    }

    public function getArrTxTipo($tx_tipo = '') {
        $arr = array(
            'ligar' => 'Ligar',
            'reuniao' => 'Reunião',
            'tarefa' => 'Tarefa',
            'email' => 'E-mail',
            'prazo' => 'Prazo',
            'almoco' => 'Almoço',
        );

        if (isset($arr[$tx_tipo])) {
            return $arr[$tx_tipo];
        }
        return $arr;
    }

}
