<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbusuariotenantpermissao extends CustomAbstractTableGateway {

    protected $table = 'tbusuariotenantpermissao';
    protected $_primary = 'id_usuariotenantpermissao';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table . " as utp");
        $query->leftJoin('tbpermissao as p ON p.id_permissao = utp.id_permissao', "*");

        if (!empty($params['columns'])) {
            $query->select(null)
                    ->select($params['columns']);
        }

        if (!empty($params['tx_controller'])) {
            $query->where("p.tx_controller like '{$params['tx_controller']}'");
        }

        if (!empty($params['tx_identificadorpermissao'])) {
            $query->where("p.tx_identificadorpermissao like '{$params['tx_identificadorpermissao']}'");
        }

        if (!empty($params['id_usuariotenant'])) {
            $query->where("id_usuariotenant = '{$params['id_usuariotenant']}'");
        }

        if (!empty($params['tx_tipopermissao'])) {
            if (is_array($params['tx_tipopermissao'])) {
                end($params['tx_tipopermissao']);
                $lastKey = key($params['tx_tipopermissao']);
                $wherePermissao = '';
                foreach ($params['tx_tipopermissao'] as $key => $tipoPermissao) {
                    $wherePermissao .= "tx_tipopermissao LIKE '%{$tipoPermissao}%'";

                    if ($key != $lastKey) {
                        $wherePermissao .= " OR ";
                    }
                }
                $wherePermissao = "({$wherePermissao})";
            } else {
                $wherePermissao = "(tx_tipopermissao LIKE '%{$params['tx_tipopermissao']}%')";
            }
            $query->where($wherePermissao);
        }
        
        
        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }
        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }


        return $query->fetchAll();
    }

    public function possuiPermissao($tx_identificadorpermissao, $id_usuariotenant = null, $tx_tipopermissao=null) {
        if (empty($id_usuariotenant)) {
            $id_usuariotenant = (new Tbusuariotenant())->getIdUsuarioLogado();
        }

        $isAdmin = (new Tbusuariotenant)->isAdmin();
        if ($isAdmin) {
            return true;
        }

        $data = $this->fetchAll(array(
            'id_usuariotenant' => $id_usuariotenant,
            'tx_identificadorpermissao' => $tx_identificadorpermissao,
            'tx_tipopermissao' => $tx_tipopermissao,
        ));

        if (!empty($data)) {
            return true;
        }
        return false;
    }

    public function possuiPermissaoCadastrar($tx_controller, $id_usuariotenant = null) {
        if (empty($id_usuariotenant)) {
            $id_usuariotenant = (new Tbusuariotenant())->getIdUsuarioLogado();
        }

        $isAdmin = (new Tbusuariotenant)->isAdmin();
        if ($isAdmin) {
            return true;
        }

        $data = $this->fetchAll(array(
            'id_usuariotenant' => $id_usuariotenant,
            'tx_controller' => $tx_controller,
            'tx_tipopermissao' => array('I', 'A'),
        ));

        if (!empty($data)) {
            return true;
        }
        return false;
    }

    public function possuiPermissaoVisualizar($tx_controller, $id_usuariotenant = null) {
        if (empty($id_usuariotenant)) {
            $id_usuariotenant = (new Tbusuariotenant())->getIdUsuarioLogado();
        }

        //não preciso testar aqui se é usuario admin, porque admin pode
        //cadastrar/alterar e excluir

        $data = $this->fetchAll(array(
            'id_usuariotenant' => $id_usuariotenant,
            'tx_controller' => $tx_controller,
            'tx_tipopermissao' => array('V'),
        ));

        if (!empty($data)) {
            return true;
        }
        return false;
    }

    public function possuiPermissaoExcluir($tx_controller, $id_usuariotenant = null) {
        if (empty($id_usuariotenant)) {
            $id_usuariotenant = (new Tbusuariotenant())->getIdUsuarioLogado();
        }

        $isAdmin = (new Tbusuariotenant)->isAdmin();
        if ($isAdmin) {
            return true;
        }

        $data = $this->fetchAll(array(
            'id_usuariotenant' => $id_usuariotenant,
            'tx_controller' => $tx_controller,
            'tx_tipopermissao' => array('E'),
        ));

        if (!empty($data)) {
            return true;
        }
        return false;
    }

}
