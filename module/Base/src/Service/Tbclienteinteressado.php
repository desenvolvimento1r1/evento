<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbclienteinteressado extends CustomAbstractTableGateway {

    protected $table = 'tbclienteinteressado';
    protected $_primary = 'id_clienteinteressado';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if(!empty($params)){
            $this->filterParams($params);
        }
        
        if (!empty($params['id_clienteinteressado'])) {
            $query->where("id_clienteinteressado = '{$params['id_clienteinteressado']}'");
        }

        if (!empty($params['id_cliente'])) {
            $query->where("id_cliente = '{$params['id_cliente']}'");
        }

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['st_origemcliente'])) {
            $query->where("st_origemcliente = '{$params['st_origemcliente']}'");
        }

        if (!empty($params['limit'])) {
            $query->limit($params['limit']);
        }

        return $query->fetchAll();
    }

}
