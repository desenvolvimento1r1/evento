<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbtenant extends CustomAbstractTableGateway {

    protected $table = 'tbtenant';
    protected $_primary = 'id_tenant';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        return $query->fetchAll();
    }

    public static function getIdTenant() {
        $ContainerUsuarioLogado = new \Zend\Session\Container('tenant');
        $seesionAdmin = $ContainerUsuarioLogado->getArrayCopy();
        if (empty($seesionAdmin)) {
            return 0;
        }
        return (int) $seesionAdmin['id_tenant'];
    }

}
