<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbexemplo extends CustomAbstractTableGateway {

    protected $table = 'tbexemplo';
    protected $_primary = 'id_exemplo';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }
   
    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        return $query->fetchAll();
    }

}
