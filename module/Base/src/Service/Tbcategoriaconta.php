<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbcategoriaconta extends CustomAbstractTableGateway {

    protected $table = 'tbcategoriaconta';
    protected $_primary = 'id_categoriaconta';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }

        if (!empty($params['id_categoriaconta'])) {
            $query->where("id_categoriaconta = '{$params['id_categoriaconta']}'");
        }

        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        if (!empty($params['tx_categoria_equal'])) {
            $query->where("tx_categoria LIKE '{$params['tx_categoria_equal']}'");
        }

        return $query->fetchAll();
    }

    public function verificaInsereCategoria($tx_categoria) {
        $categoria = $this->fetchAll(array('tx_categoria_equal' => $tx_categoria));


        if (!empty($categoria)) {
            return $categoria[0]['id_categoriaconta'];
        } else {
            $Default = (new Connection)->getConnection();
            $id = $this->save(['tx_categoria' => $tx_categoria], $Default);
            return $id;
        }
    }

}
