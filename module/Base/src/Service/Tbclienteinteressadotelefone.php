<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbclienteinteressadotelefone extends CustomAbstractTableGateway {

    protected $table = 'tbclienteinteressadotelefone';
    protected $_primary = 'id_clienteinteressadotelefone';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (!empty($params['id_clienteinteressado'])) {
            $query->where("id_clienteinteressado = '{$params['id_clienteinteressado']}'");
        }

        
        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }
        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }

        
        return $query->fetchAll();
    }

    public static function getArrTiposTelefones() {
        return [
            'fixo_so' => 'FIXO (sem operadora)',
            'cel_so' => 'CELULAR (sem operadora)',
            'fixo_vivo' => 'VIVO(FIXO)',
            'cel_vivo' => 'VIVO(CELULAR)',
            'fixo_tim' => 'TIM(FIXO)',
            'cel_tim' => 'TIM(CELULAR)',
            'fixo_claro' => 'CLARO(FIXO)',
            'cel_claro' => 'CLARO(CELULAR)',
            'fixo_oi' => 'OI(FIXO)',
            'cel_oi' => 'OI(CELULAR)',
            'fixo_embratel' => 'EMBRATEL(FIXO)',
            'cel_embratel' => 'EMBRATEL(CELULAR)',
            'fixo_gvt' => 'GVT(FIXO)',
            'cel_fvt' => 'GVT(CELULAR)',
        ];
    }

}
