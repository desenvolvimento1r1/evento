<?php

namespace Base\Service;

use Zend\Config\CustomAbstractTableGateway;

class Tbfuncionarioferias extends CustomAbstractTableGateway {

    protected $table = 'tbfuncionarioferias';
    protected $_primary = 'id_funcionarioferias';
    protected $sql;

    public function __construct() {
        $this->sql = (new Connection)->getConnection();
    }

    public function fetchAll($params = array()) {
        $db = $this->sql;
        $query = $db->from($this->table);

        if (!empty($params['id_funcionario'])) {
            $query->where("id_funcionario = '{$params['id_funcionario']}'");
        }

        if (empty($params['id_tenant'])) {
            $params['id_tenant'] = (new \Zend\Session\Container('tenant'))->id_tenant;
        }
        if (!empty($params['id_tenant'])) {
            $query->where("id_tenant = '{$params['id_tenant']}'");
        }


        return $query->fetchAll();
    }

}
