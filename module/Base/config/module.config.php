<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonPortal for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Base;

use Base\Controller\IndexController;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'base' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/base[/:controller[/:action]]',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => InvokableFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
//        'not_found_template' => 'error/404-admin',
//        'exception_template' => 'error/index-admin',
        'template_map' => [
//            'layout/base' => __DIR__ . '/../view/layout/base.phtml',
//            'layout/login' => __DIR__ . '/../view/layout/login.phtml',
//            'admin/index/index' => __DIR__ . '/../view/base/index/index.phtml',
//            'error/404-admin' => __DIR__ . '/../view/error/404.phtml',
//            'error/index-admin' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
