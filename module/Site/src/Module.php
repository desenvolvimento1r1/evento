<?php

/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Site;

use Zend\Mvc\MvcEvent;

class Module {

    const VERSION = '3.0.3-dev';

    public function getConfig() {
        return include __DIR__ . '/../config/module.config.php';
    }

    function onBootstrap(MvcEvent $e) {
        $app = $e->getApplication();
        $evt = $app->getEventManager();
        $evt->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 100);
    }

    function onDispatchError(MvcEvent $e) {
        $module = $e->getRouteMatch()->getMatchedRouteName();
        
        $vm = $e->getViewModel();
        $vm->setTemplate('error/404' . '-' . $module);
    }

}
