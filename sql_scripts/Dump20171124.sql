-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: evento
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `tbpermissao`
--

LOCK TABLES `tbpermissao` WRITE;
/*!40000 ALTER TABLE `tbpermissao` DISABLE KEYS */;
INSERT INTO `tbpermissao` VALUES (1,'Usuários','Cadastrar/Alterar Usuários','usuario','I|A'),(2,'Usuários','Excluir Usuário','usuario','E'),(3,'Usuários','Somente Visualizar','usuario','V');
/*!40000 ALTER TABLE `tbpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbtenant`
--

LOCK TABLES `tbtenant` WRITE;
/*!40000 ALTER TABLE `tbtenant` DISABLE KEYS */;
INSERT INTO `tbtenant` VALUES (1,'Usuario Teste','24.381.697/0001-56');
/*!40000 ALTER TABLE `tbtenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbusuariotenant`
--

LOCK TABLES `tbusuariotenant` WRITE;
/*!40000 ALTER TABLE `tbusuariotenant` DISABLE KEYS */;
INSERT INTO `tbusuariotenant` VALUES (2,'Caique Galvanin Rodrigues 2','caique.galvanin@gmail.com','caique','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S'),(17,'teste permissão','permissao@gmail.com','permissao','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S');
/*!40000 ALTER TABLE `tbusuariotenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbusuariotenantpermissao`
--

LOCK TABLES `tbusuariotenantpermissao` WRITE;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` DISABLE KEYS */;
INSERT INTO `tbusuariotenantpermissao` VALUES (11,2,1,1),(12,2,1,2),(17,17,1,2),(18,17,1,3);
/*!40000 ALTER TABLE `tbusuariotenantpermissao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-24  0:17:20
