-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: evento
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbbancoconta`
--

DROP TABLE IF EXISTS `tbbancoconta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbbancoconta` (
  `id_bancoconta` bigint(20) NOT NULL AUTO_INCREMENT,
  `arr_banco` int(20) NOT NULL,
  `tx_agencia` varchar(20) NOT NULL,
  `tx_conta` varchar(20) NOT NULL,
  `st_ativo` char(1) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_bancoconta`),
  KEY `fk_tenant_idx` (`id_tenant`),
  CONSTRAINT `fk_tenant` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbbancoconta`
--

LOCK TABLES `tbbancoconta` WRITE;
/*!40000 ALTER TABLE `tbbancoconta` DISABLE KEYS */;
INSERT INTO `tbbancoconta` VALUES (3,18,'0552','13718-4','A',1),(4,1,'123','dwadaw','A',1),(5,8,'0555','223444','I',1);
/*!40000 ALTER TABLE `tbbancoconta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcategoriaconta`
--

DROP TABLE IF EXISTS `tbcategoriaconta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcategoriaconta` (
  `id_categoriaconta` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_categoria` varchar(100) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_categoriaconta`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbcategoriaconta_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcategoriaconta`
--

LOCK TABLES `tbcategoriaconta` WRITE;
/*!40000 ALTER TABLE `tbcategoriaconta` DISABLE KEYS */;
INSERT INTO `tbcategoriaconta` VALUES (1,'cat',1),(4,'1',1),(5,'gastos gerais',1);
/*!40000 ALTER TABLE `tbcategoriaconta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcliente`
--

DROP TABLE IF EXISTS `tbcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcliente` (
  `id_cliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(255) NOT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `st_tipopessoa` char(1) NOT NULL,
  `tx_cpf` varchar(14) DEFAULT NULL,
  `tx_cnpj` varchar(18) DEFAULT NULL,
  `tx_rg` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  `tx_sexo` char(1) DEFAULT NULL,
  `tx_observacao` text,
  `id_clienteinteressado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbcliente_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcliente`
--

LOCK TABLES `tbcliente` WRITE;
/*!40000 ALTER TABLE `tbcliente` DISABLE KEYS */;
INSERT INTO `tbcliente` VALUES (18,'Caique Galvanin Rodrigues','2017-11-14','F','344.990.358-64',NULL,'4134755-5',1,'M','teste',NULL),(23,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(24,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(25,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(26,'askdopas',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(27,'askdopas',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(28,'cc',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(29,'1',NULL,'1',NULL,NULL,NULL,1,NULL,NULL,NULL),(30,'1',NULL,'1',NULL,NULL,NULL,1,NULL,NULL,NULL),(32,'cc',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(34,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(35,'teste',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(36,'teste',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(46,'Outro teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa',NULL),(47,'Outro teste teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa',NULL),(48,'Outro teste teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa',NULL),(49,'Outro teste teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa',NULL);
/*!40000 ALTER TABLE `tbcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteemail`
--

DROP TABLE IF EXISTS `tbclienteemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteemail` (
  `id_clienteemail` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_email` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteemail`),
  KEY `id_cliente` (`id_cliente`),
  KEY `tbclienteemail_ibfk_2` (`id_tenant`),
  CONSTRAINT `tbclienteemail_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteemail_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteemail`
--

LOCK TABLES `tbclienteemail` WRITE;
/*!40000 ALTER TABLE `tbclienteemail` DISABLE KEYS */;
INSERT INTO `tbclienteemail` VALUES (82,'dadda@awdwa.com',24,1),(83,'dadda@awdwa.com',25,1),(84,'awdwad@wadwa.dcom',35,1),(88,'caique.galvanin@gmail.com',18,1),(89,'caique.galvanin2@gmail.com',18,1),(90,'caique.rodrigues@hotmail.com.br',18,1),(91,'123123aweawd@awdaw.com',46,1),(92,'awdad2d2a@wadwad.com',46,1),(93,'123123aweawd@awdaw.com',47,1),(94,'awdad2d2a@wadwad.com',47,1),(95,'123123aweawd@awdaw.com',48,1),(96,'awdad2d2a@wadwad.com',48,1),(97,'123123aweawd@awdaw.com',49,1),(98,'awdad2d2a@wadwad.com',49,1);
/*!40000 ALTER TABLE `tbclienteemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteendereco`
--

DROP TABLE IF EXISTS `tbclienteendereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteendereco` (
  `id_clienteendereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_endereco` varchar(255) DEFAULT NULL,
  `tx_bairro` varchar(100) DEFAULT NULL,
  `tx_uf` char(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cep` varchar(50) DEFAULT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `tx_observacao` text,
  `tx_complemento` varchar(100) DEFAULT NULL,
  `tx_cidade` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteendereco`),
  KEY `id_cliente` (`id_cliente`),
  KEY `tbclienteendereco_ibfk_2` (`id_tenant`),
  CONSTRAINT `tbclienteendereco_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteendereco_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteendereco`
--

LOCK TABLES `tbclienteendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteendereco` DISABLE KEYS */;
INSERT INTO `tbclienteendereco` VALUES (58,'Praça da Sé','Sé','SP','a','01001-000',35,'awdwad','lado ímpar','São Paulo',1),(61,'Rua Júlio Mori','Jardim Ouro Verde','SP','530','19906-000',18,'obs1','edicula','Ourinhos',1),(62,'Rua Washington Luis','Centro','SP','1500','18950-000',18,'ob2','Casa','Ipaussu',1),(63,NULL,NULL,NULL,NULL,NULL,18,NULL,NULL,NULL,1),(64,'Praça da Sé','Sé','SP','amdiao','01001-000',46,'awoidja','lado ímpar','São Paulo',1),(65,'eafeaf','eaf','SP','aef','18950-000',46,'eaf','eaf','Ipaussu',1),(66,'Praça da Sé','Sé','SP','amdiao','01001-000',47,'awoidja','lado ímpar','São Paulo',1),(67,'eafeaf','eaf','SP','aef','18950-000',47,'eaf','eaf','Ipaussu',1),(68,'Praça da Sé','Sé','SP','amdiao','01001-000',48,'awoidja','lado ímpar','São Paulo',1),(69,'eafeaf','eaf','SP','aef','18950-000',48,'eaf','eaf','Ipaussu',1),(70,'Praça da Sé','Sé','SP','amdiao','01001-000',49,'awoidja','lado ímpar','São Paulo',1),(71,'eafeaf','eaf','SP','aef','18950-000',49,'eaf','eaf','Ipaussu',1);
/*!40000 ALTER TABLE `tbclienteendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressado`
--

DROP TABLE IF EXISTS `tbclienteinteressado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressado` (
  `id_clienteinteressado` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(255) NOT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `st_tipopessoa` char(1) NOT NULL,
  `tx_cpf` varchar(14) DEFAULT NULL,
  `tx_cnpj` varchar(18) DEFAULT NULL,
  `tx_rg` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  `tx_sexo` char(1) DEFAULT NULL,
  `tx_observacao` text,
  `dt_primeirocontato` datetime DEFAULT NULL,
  `tx_origemcontato` varchar(255) DEFAULT NULL,
  `tx_objetivocontato` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressado_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressado`
--

LOCK TABLES `tbclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbclienteinteressado` VALUES (8,'Outro teste teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa','2017-11-26 13:35:00',NULL,NULL),(9,'Outro teste','0000-00-00','F','494.949.494-94',NULL,'12312x',1,'F',NULL,'2017-11-25 11:30:00',NULL,NULL),(10,'Outro teste','1994-01-06','F','344.990.358-64',NULL,'12312x',1,'F',NULL,'2017-11-25 11:30:00',NULL,NULL),(11,'Outro teste','0000-00-00','F','494.949.494-94',NULL,'12312x',1,'F',NULL,'1970-01-01 00:00:00',NULL,NULL),(12,'awdwadwa',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadoemail`
--

DROP TABLE IF EXISTS `tbclienteinteressadoemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadoemail` (
  `id_clientelinteressadoemail` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_email` varchar(100) NOT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clientelinteressadoemail`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadoemail_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadoemail_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadoemail`
--

LOCK TABLES `tbclienteinteressadoemail` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoemail` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadoemail` VALUES (10,'123123aweawd@awdaw.com',8,1),(11,'awdad2d2a@wadwad.com',8,1);
/*!40000 ALTER TABLE `tbclienteinteressadoemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadoendereco`
--

DROP TABLE IF EXISTS `tbclienteinteressadoendereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadoendereco` (
  `id_clienteinteressadoendereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_endereco` varchar(255) DEFAULT NULL,
  `tx_bairro` varchar(100) DEFAULT NULL,
  `tx_uf` char(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cep` varchar(50) DEFAULT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `tx_observacao` text,
  `tx_complemento` varchar(100) DEFAULT NULL,
  `tx_cidade` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteinteressadoendereco`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadoendereco_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadoendereco_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadoendereco`
--

LOCK TABLES `tbclienteinteressadoendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadoendereco` VALUES (10,'Praça da Sé','Sé','SP','amdiao','01001-000',8,'awoidja','lado ímpar','São Paulo',1),(11,'eafeaf','eaf','SP','aef','18950-000',8,'eaf','eaf','Ipaussu',1);
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadotelefone`
--

DROP TABLE IF EXISTS `tbclienteinteressadotelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadotelefone` (
  `id_clienteinteressadotelefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_telefone` varchar(100) NOT NULL,
  `tx_tipo` varchar(100) NOT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteinteressadotelefone`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadotelefone_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadotelefone_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadotelefone`
--

LOCK TABLES `tbclienteinteressadotelefone` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadotelefone` VALUES (2,'(12) 31231-23','fixo_so',9,1),(4,'(12) 31231-23','fixo_so',11,1),(9,'(12) 31231-23','fixo_so',10,1),(19,'(12) 31231-23','fixo_so',8,1);
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclientetelefone`
--

DROP TABLE IF EXISTS `tbclientetelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclientetelefone` (
  `id_clientetelefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_telefone` varchar(100) NOT NULL,
  `tx_tipo` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clientetelefone`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclientetelefone_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclientetelefone_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclientetelefone`
--

LOCK TABLES `tbclientetelefone` WRITE;
/*!40000 ALTER TABLE `tbclientetelefone` DISABLE KEYS */;
INSERT INTO `tbclientetelefone` VALUES (63,'a111111','fixo_so',35,1),(70,'(14) 33441-447','fixo_so',18,1),(71,'(14) 99760-6649','fixo_so',18,1),(72,'(12) 31231-23','fixo_so',46,1),(73,'(12) 31231-23','fixo_so',47,1),(74,'(12) 31231-23','fixo_so',48,1),(75,'(12) 31231-23','fixo_so',49,1);
/*!40000 ALTER TABLE `tbclientetelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcontareceber`
--

DROP TABLE IF EXISTS `tbcontareceber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcontareceber` (
  `id_contareceber` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_descricao` varchar(200) NOT NULL,
  `id_categoriaconta` bigint(20) DEFAULT NULL,
  `id_bancoconta` bigint(20) NOT NULL,
  `dt_competencia` date NOT NULL,
  `dt_vencimento` date NOT NULL,
  `vl_total` decimal(15,2) NOT NULL,
  `st_tipomulta` char(1) DEFAULT NULL,
  `vl_multa` decimal(15,4) DEFAULT NULL,
  `st_tipomora` char(1) DEFAULT NULL,
  `vl_mora` decimal(15,4) DEFAULT NULL,
  `st_tiporepetir` varchar(20) DEFAULT NULL,
  `st_lancarcomoparcelamento` char(1) DEFAULT 'N',
  `nr_ocorrencias` int(10) DEFAULT NULL,
  `tx_observacao` text,
  `id_contareceberorigem` bigint(20) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_contareceber`),
  KEY `id_tenant` (`id_tenant`),
  KEY `id_bancoconta` (`id_bancoconta`),
  CONSTRAINT `tbcontareceber_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbcontareceber_ibfk_2` FOREIGN KEY (`id_bancoconta`) REFERENCES `tbbancoconta` (`id_bancoconta`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcontareceber`
--

LOCK TABLES `tbcontareceber` WRITE;
/*!40000 ALTER TABLE `tbcontareceber` DISABLE KEYS */;
INSERT INTO `tbcontareceber` VALUES (5,'desc',1,3,'2017-12-26','2017-12-26',12.34,'R',0.0100,'P',0.0123,'mes','N',2,'obs',NULL,1),(6,'conta 6',1,3,'2017-12-26','2017-12-26',12.34,'R',0.0100,'P',0.0123,NULL,'N',NULL,'obs',NULL,1),(10,'conta 10',5,3,'2017-12-26','2017-12-26',12.34,'R',0.0100,'P',0.0123,'mes','N',2,'obs',NULL,1),(11,'conta 11',5,3,'2017-12-26','2018-01-26',12.34,'R',0.0100,'P',0.0123,'mes','N',2,'obs',10,1),(12,'desc',1,3,'2017-12-26','2018-01-26',12.34,'R',0.0100,'P',0.0123,'mes','N',2,'obs',10,1),(13,'teste',5,3,'2018-05-27','2018-05-28',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',NULL,1),(14,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(15,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(16,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(17,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(18,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(19,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(20,'teste',5,3,'2018-05-27','2018-05-29',12.34,'R',0.0000,'R',0.0000,'diaria','N',7,'nada',13,1),(21,'teste 2',1,4,'2018-05-28','2018-05-28',0.01,'R',NULL,'R',NULL,'mes','N',2,'t',NULL,1),(22,'teste 2',1,4,'2018-05-28','2018-06-28',0.01,'R',NULL,'R',NULL,'mes','N',2,'t',21,1),(23,'teste 2',1,4,'2018-05-28','2018-06-28',0.01,'R',NULL,'R',NULL,'mes','N',2,'t',21,1),(55,'teste',5,3,'2018-05-27','2018-05-27',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',NULL,1),(56,'teste',5,3,'2018-05-27','2018-05-28',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(57,'teste',5,3,'2018-05-27','2018-05-29',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(58,'teste',5,3,'2018-05-27','2018-05-30',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(59,'teste',5,3,'2018-05-27','2018-05-31',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(60,'teste',5,3,'2018-05-27','2018-06-01',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(61,'teste',5,3,'2018-05-27','2018-06-02',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(62,'teste',5,3,'2018-05-27','2018-06-03',0.01,'R',0.0100,'R',0.0100,'diaria','N',7,'1',55,1),(63,'teste',4,3,'2018-05-28','2018-05-28',0.01,'R',0.0000,'R',0.0000,'diaria','S',3,NULL,NULL,1),(64,'(1/3)teste',4,3,'2018-05-28','2018-05-29',0.01,'R',0.0000,'R',0.0000,'diaria','N',3,NULL,63,1),(65,'(2/3)(1/3)teste',4,3,'2018-05-28','2018-05-30',0.01,'R',0.0000,'R',0.0000,'diaria','N',3,NULL,63,1),(66,'(3/3)(2/3)(1/3)teste',4,3,'2018-05-28','2018-05-31',0.01,'R',0.0000,'R',0.0000,'diaria','N',3,NULL,63,1),(67,'a',1,3,'2018-05-28','2018-05-28',0.01,'R',NULL,'R',NULL,'diaria','S',5,NULL,NULL,1),(68,'(1/5)a',1,3,'2018-05-28','2018-05-29',0.01,'R',NULL,'R',NULL,'diaria','N',5,NULL,67,1),(69,'(2/5)a',1,3,'2018-05-28','2018-05-30',0.01,'R',NULL,'R',NULL,'diaria','N',5,NULL,67,1),(70,'(3/5)a',1,3,'2018-05-28','2018-05-31',0.01,'R',NULL,'R',NULL,'diaria','N',5,NULL,67,1),(71,'(4/5)a',1,3,'2018-05-28','2018-06-01',0.01,'R',NULL,'R',NULL,'diaria','N',5,NULL,67,1),(72,'(5/5)a',1,3,'2018-05-28','2018-06-02',0.01,'R',NULL,'R',NULL,'diaria','N',5,NULL,67,1);
/*!40000 ALTER TABLE `tbcontareceber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcontareceberanexo`
--

DROP TABLE IF EXISTS `tbcontareceberanexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcontareceberanexo` (
  `id_contareceberanexo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_folder` varchar(255) NOT NULL,
  `tx_nomeservidor` varchar(255) NOT NULL,
  `tx_nomearquivo` varchar(255) NOT NULL,
  `id_contareceber` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_contareceberanexo`),
  KEY `id_contareceber` (`id_contareceber`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbcontareceberanexo_ibfk_1` FOREIGN KEY (`id_contareceber`) REFERENCES `tbcontareceber` (`id_contareceber`),
  CONSTRAINT `tbcontareceberanexo_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcontareceberanexo`
--

LOCK TABLES `tbcontareceberanexo` WRITE;
/*!40000 ALTER TABLE `tbcontareceberanexo` DISABLE KEYS */;
INSERT INTO `tbcontareceberanexo` VALUES (1,'uploads/conta-receber/anexos/c4ca4238a0b923820dcc509a6f75849b','5a4251c5004e13.59741849.txt','####### IDEIAS ######## - Copia.txt',5,1),(2,'uploads/conta-receber/anexos/c4ca4238a0b923820dcc509a6f75849b','5a4251c500d707.38583202.txt','####### IDEIAS ########.txt',5,1),(3,'uploads/conta-receber/anexos/c4ca4238a0b923820dcc509a6f75849b','5a7ef38a3c61d3.79222788.jpg','Screen(02_05-21-27)-0000.jpg',10,1);
/*!40000 ALTER TABLE `tbcontareceberanexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinteracaoclienteinteressado`
--

DROP TABLE IF EXISTS `tbinteracaoclienteinteressado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinteracaoclienteinteressado` (
  `id_interacaoclienteinteressado` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `tx_interacao` text NOT NULL,
  `dt_datahora` datetime NOT NULL,
  `tx_tipo` varchar(255) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `id_usuariotenant_criadopor` bigint(20) NOT NULL,
  `id_usuariotenant_responsavelpor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_interacaoclienteinteressado`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  KEY `tbinteracaoclienteinteressado_ibfk_4` (`id_usuariotenant_criadopor`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_3` FOREIGN KEY (`id_usuariotenant_criadopor`) REFERENCES `tbusuariotenant` (`id_usuariotenant`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_4` FOREIGN KEY (`id_usuariotenant_criadopor`) REFERENCES `tbusuariotenant` (`id_usuariotenant`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinteracaoclienteinteressado`
--

LOCK TABLES `tbinteracaoclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbinteracaoclienteinteressado` VALUES (44,8,'teste','2017-12-10 19:22:00','ligar',1,2,NULL),(45,8,'teste','2017-12-10 19:22:00','tarefa',1,2,2);
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinteracaoclienteinteressadoanexo`
--

DROP TABLE IF EXISTS `tbinteracaoclienteinteressadoanexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinteracaoclienteinteressadoanexo` (
  `id_interacaoclienteinteressadoanexo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_folder` varchar(500) NOT NULL,
  `tx_nomeservidor` varchar(500) NOT NULL,
  `id_interacaoclienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `tx_nomearquivo` varchar(500) NOT NULL,
  PRIMARY KEY (`id_interacaoclienteinteressadoanexo`),
  KEY `id_tenant` (`id_tenant`),
  KEY `tbclienteinteressadoanexo_ibfk_1_idx` (`id_interacaoclienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressadoanexo_ibfk_1` FOREIGN KEY (`id_interacaoclienteinteressado`) REFERENCES `tbinteracaoclienteinteressado` (`id_interacaoclienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressadoanexo_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinteracaoclienteinteressadoanexo`
--

LOCK TABLES `tbinteracaoclienteinteressadoanexo` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` DISABLE KEYS */;
INSERT INTO `tbinteracaoclienteinteressadoanexo` VALUES (1,'uploads/crm-interacao/anexos/c4ca4238a0b923820dcc509a6f75849b','5a7ed925b21cd4.87713084.jpg',45,1,'Screen(02_05-21-27)-0000.jpg'),(2,'uploads/crm-interacao/anexos/c4ca4238a0b923820dcc509a6f75849b','5a7ed925b23653.48526735.jpg',45,1,'Screen(02_05-21-28)-0001.jpg');
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpermissao`
--

DROP TABLE IF EXISTS `tbpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpermissao` (
  `id_permissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_panel` varchar(255) NOT NULL,
  `tx_nomepermissao` varchar(100) NOT NULL,
  `tx_controller` varchar(100) NOT NULL,
  `tx_tipopermissao` varchar(10) NOT NULL,
  `tx_identificadorpermissao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpermissao`
--

LOCK TABLES `tbpermissao` WRITE;
/*!40000 ALTER TABLE `tbpermissao` DISABLE KEYS */;
INSERT INTO `tbpermissao` VALUES (1,'Usuários','Cadastrar/Alterar Usuários','usuario','I|A',''),(2,'Usuários','Excluir Usuário','usuario','E',''),(3,'Usuários','Somente Visualizar','usuario','V',''),(4,'Cliente','Cadastrar/Alterar Clientes','cliente','I|A',''),(5,'Cliente','Excluir Cliente','cliente','E',''),(6,'Cliente','Somente Visualizar','cliente','V',''),(7,'Usuários','Alterar Permissões dos Usuários','usuario','R','alterar-permissoes-usuarios'),(10,'Cadastro de Interessados','Cadastro de Interessados','crm-interessado','I|A',NULL),(11,'Cadastro de Interessados','Excluir Cadastro de Interessados','crm-interessado','E',NULL),(12,'Cadastro de Interessados','Somente Visualizar','crm-interessado','V',NULL),(13,'Cadastro de Interessados','Cadastrar Interação','crm-interessado','I|A','cadastrar-interacao-crm-interessado'),(14,'Cadastro de Interessados','Excluir Interação','crm-interessado','E','excluir-interacao-crm-interessado'),(16,'Cadastro de Interessados','Somente Visualizar Interação','crm-interessado','V','visualizar-interacao-crm-interessado');
/*!40000 ALTER TABLE `tbpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtenant`
--

DROP TABLE IF EXISTS `tbtenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtenant` (
  `id_tenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomefantasia` varchar(255) NOT NULL,
  `tx_cnpj` varchar(18) NOT NULL,
  PRIMARY KEY (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtenant`
--

LOCK TABLES `tbtenant` WRITE;
/*!40000 ALTER TABLE `tbtenant` DISABLE KEYS */;
INSERT INTO `tbtenant` VALUES (1,'Usuario Teste','24.381.697/0001-56');
/*!40000 ALTER TABLE `tbtenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuario`
--

DROP TABLE IF EXISTS `tbusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuario` (
  `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomeusuario` varchar(255) NOT NULL,
  `tx_email` varchar(255) DEFAULT NULL,
  `tx_login` varchar(150) NOT NULL,
  `tx_senha` varchar(100) DEFAULT NULL,
  `fb_userid` varchar(200) DEFAULT NULL,
  `st_tipologin` char(1) NOT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuario`
--

LOCK TABLES `tbusuario` WRITE;
/*!40000 ALTER TABLE `tbusuario` DISABLE KEYS */;
INSERT INTO `tbusuario` VALUES (1,'Caique Galvanin','caique.galvanin@gmail.com','caique','123',NULL,''),(2,'HudFastswitch','','',NULL,'100810150678770','F');
/*!40000 ALTER TABLE `tbusuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenant`
--

DROP TABLE IF EXISTS `tbusuariotenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenant` (
  `id_usuariotenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomeusuario` varchar(255) NOT NULL,
  `tx_email` varchar(255) DEFAULT NULL,
  `tx_login` varchar(150) NOT NULL,
  `tx_senha` varchar(100) DEFAULT NULL,
  `fb_userid` varchar(200) DEFAULT NULL,
  `st_tipologin` char(1) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `st_ativo` char(1) NOT NULL DEFAULT 'S',
  `st_admin` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenant_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenant`
--

LOCK TABLES `tbusuariotenant` WRITE;
/*!40000 ALTER TABLE `tbusuariotenant` DISABLE KEYS */;
INSERT INTO `tbusuariotenant` VALUES (2,'Caique Galvanin Rodrigues 2','caique.galvanin@gmail.com','caique','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S','S'),(3,'a','a@a.com.br','123','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,NULL,1,'S','N');
/*!40000 ALTER TABLE `tbusuariotenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenantpermissao`
--

DROP TABLE IF EXISTS `tbusuariotenantpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenantpermissao` (
  `id_usuariotenantpermissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuariotenant` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `id_permissao` bigint(20) NOT NULL,
  PRIMARY KEY (`id_usuariotenantpermissao`),
  KEY `id_usuariotenant` (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  KEY `id_permissao` (`id_permissao`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_1` FOREIGN KEY (`id_usuariotenant`) REFERENCES `tbusuariotenant` (`id_usuariotenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_3` FOREIGN KEY (`id_permissao`) REFERENCES `tbpermissao` (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenantpermissao`
--

LOCK TABLES `tbusuariotenantpermissao` WRITE;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` DISABLE KEYS */;
INSERT INTO `tbusuariotenantpermissao` VALUES (1,3,1,10),(2,3,1,11),(3,3,1,16);
/*!40000 ALTER TABLE `tbusuariotenantpermissao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-27 21:46:13
