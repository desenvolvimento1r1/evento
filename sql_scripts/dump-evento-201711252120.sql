-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: evento
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbcliente`
--

DROP TABLE IF EXISTS `tbcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcliente` (
  `id_cliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(255) NOT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `st_tipopessoa` char(1) NOT NULL,
  `tx_cpf` varchar(14) DEFAULT NULL,
  `tx_cnpj` varchar(18) DEFAULT NULL,
  `tx_rg` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  `tx_sexo` char(1) DEFAULT NULL,
  `tx_observacao` text,
  PRIMARY KEY (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbcliente_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcliente`
--

LOCK TABLES `tbcliente` WRITE;
/*!40000 ALTER TABLE `tbcliente` DISABLE KEYS */;
INSERT INTO `tbcliente` VALUES (18,'Caique Galvanin Rodrigues','2017-11-25','F','344.990.358-64',NULL,'4134755-5',1,'M',NULL);
/*!40000 ALTER TABLE `tbcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteemail`
--

DROP TABLE IF EXISTS `tbclienteemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteemail` (
  `id_clienteemail` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_email` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_clienteemail`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteemail_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteemail_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteemail`
--

LOCK TABLES `tbclienteemail` WRITE;
/*!40000 ALTER TABLE `tbclienteemail` DISABLE KEYS */;
INSERT INTO `tbclienteemail` VALUES (1,'caique.galvanin@gmail.com',18,1),(2,'caique.galvanin2@gmail.com',18,1),(3,'caique.rodrigues@hotmail.com.br',18,1);
/*!40000 ALTER TABLE `tbclienteemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteendereco`
--

DROP TABLE IF EXISTS `tbclienteendereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteendereco` (
  `id_clienteendereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_endereco` varchar(255) DEFAULT NULL,
  `tx_bairro` varchar(100) DEFAULT NULL,
  `tx_uf` char(2) NOT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cep` varchar(50) DEFAULT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `tx_observacao` text,
  `tx_complemento` varchar(100) DEFAULT NULL,
  `tx_cidade` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_clienteendereco`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteendereco_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteendereco_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteendereco`
--

LOCK TABLES `tbclienteendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteendereco` DISABLE KEYS */;
INSERT INTO `tbclienteendereco` VALUES (1,'Rua Júlio Mori','Jardim Ouro Verde','SP','530','19906-000',18,'Caso não encontre, falar com Fatima','edicula','Ourinhos',1),(2,'Rua Washington Luis','Centro','SP','1500','18950-000',18,'Casa da mãe do Caique, qualquer coisa, falar com Renata','Casa','Ipaussu',1);
/*!40000 ALTER TABLE `tbclienteendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclientetelefone`
--

DROP TABLE IF EXISTS `tbclientetelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclientetelefone` (
  `id_clientetelefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_telefone` varchar(100) NOT NULL,
  `tx_tipo` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clientetelefone`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclientetelefone_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclientetelefone_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclientetelefone`
--

LOCK TABLES `tbclientetelefone` WRITE;
/*!40000 ALTER TABLE `tbclientetelefone` DISABLE KEYS */;
INSERT INTO `tbclientetelefone` VALUES (1,'(14) 33441-447','fixo_vivo',18,1),(2,'(14) 99760-6649','cel_vivo',18,1);
/*!40000 ALTER TABLE `tbclientetelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpermissao`
--

DROP TABLE IF EXISTS `tbpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpermissao` (
  `id_permissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_panel` varchar(255) NOT NULL,
  `tx_nomepermissao` varchar(100) NOT NULL,
  `tx_controller` varchar(100) NOT NULL,
  `tx_tipopermissao` varchar(10) NOT NULL,
  `tx_identificadorpermissao` varchar(255) NOT NULL,
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpermissao`
--

LOCK TABLES `tbpermissao` WRITE;
/*!40000 ALTER TABLE `tbpermissao` DISABLE KEYS */;
INSERT INTO `tbpermissao` VALUES (1,'Usuários','Cadastrar/Alterar Usuários','usuario','I|A',''),(2,'Usuários','Excluir Usuário','usuario','E',''),(3,'Usuários','Somente Visualizar','usuario','V',''),(4,'Cliente','Cadastrar/Alterar Clientes','cliente','I|A',''),(5,'Cliente','Excluir Cliente','cliente','E',''),(6,'Cliente','Somente Visualizar','cliente','V',''),(7,'Usuários','Alterar Permissões dos Usuários','usuario','R','alterar-permissoes-usuarios');
/*!40000 ALTER TABLE `tbpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtenant`
--

DROP TABLE IF EXISTS `tbtenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtenant` (
  `id_tenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomefantasia` varchar(255) NOT NULL,
  `tx_cnpj` varchar(18) NOT NULL,
  PRIMARY KEY (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtenant`
--

LOCK TABLES `tbtenant` WRITE;
/*!40000 ALTER TABLE `tbtenant` DISABLE KEYS */;
INSERT INTO `tbtenant` VALUES (1,'Usuario Teste','24.381.697/0001-56');
/*!40000 ALTER TABLE `tbtenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenant`
--

DROP TABLE IF EXISTS `tbusuariotenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenant` (
  `id_usuariotenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomeusuario` varchar(255) NOT NULL,
  `tx_email` varchar(255) DEFAULT NULL,
  `tx_login` varchar(150) NOT NULL,
  `tx_senha` varchar(100) DEFAULT NULL,
  `fb_userid` varchar(200) DEFAULT NULL,
  `st_tipologin` char(1) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `st_ativo` char(1) NOT NULL DEFAULT 'S',
  `st_admin` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenant_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenant`
--

LOCK TABLES `tbusuariotenant` WRITE;
/*!40000 ALTER TABLE `tbusuariotenant` DISABLE KEYS */;
INSERT INTO `tbusuariotenant` VALUES (2,'Caique Galvanin Rodrigues 2','caique.galvanin@gmail.com','caique','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S','S'),(20,'',NULL,'',NULL,NULL,'',1,'N','N');
/*!40000 ALTER TABLE `tbusuariotenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenantpermissao`
--

DROP TABLE IF EXISTS `tbusuariotenantpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenantpermissao` (
  `id_usuariotenantpermissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuariotenant` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `id_permissao` bigint(20) NOT NULL,
  PRIMARY KEY (`id_usuariotenantpermissao`),
  KEY `id_usuariotenant` (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  KEY `id_permissao` (`id_permissao`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_1` FOREIGN KEY (`id_usuariotenant`) REFERENCES `tbusuariotenant` (`id_usuariotenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_3` FOREIGN KEY (`id_permissao`) REFERENCES `tbpermissao` (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenantpermissao`
--

LOCK TABLES `tbusuariotenantpermissao` WRITE;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'evento'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-25 21:20:03
