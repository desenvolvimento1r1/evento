-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: evento
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbcliente`
--

DROP TABLE IF EXISTS `tbcliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcliente` (
  `id_cliente` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(255) NOT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `st_tipopessoa` char(1) NOT NULL,
  `tx_cpf` varchar(14) DEFAULT NULL,
  `tx_cnpj` varchar(18) DEFAULT NULL,
  `tx_rg` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  `tx_sexo` char(1) DEFAULT NULL,
  `tx_observacao` text,
  `id_clienteinteressado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbcliente_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcliente`
--

LOCK TABLES `tbcliente` WRITE;
/*!40000 ALTER TABLE `tbcliente` DISABLE KEYS */;
INSERT INTO `tbcliente` VALUES (18,'Caique Galvanin Rodrigues','2017-11-14','F','344.990.358-64',NULL,'4134755-5',1,'M','teste',NULL),(23,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(24,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(25,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(26,'askdopas',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(27,'askdopas',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(28,'cc',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(29,'1',NULL,'1',NULL,NULL,NULL,1,NULL,NULL,NULL),(30,'1',NULL,'1',NULL,NULL,NULL,1,NULL,NULL,NULL),(32,'cc',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(34,'a',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(35,'teste',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(36,'teste',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL),(46,'Outro teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa',8);
/*!40000 ALTER TABLE `tbcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteemail`
--

DROP TABLE IF EXISTS `tbclienteemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteemail` (
  `id_clienteemail` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_email` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteemail`),
  KEY `id_cliente` (`id_cliente`),
  KEY `tbclienteemail_ibfk_2` (`id_tenant`),
  CONSTRAINT `tbclienteemail_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteemail_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteemail`
--

LOCK TABLES `tbclienteemail` WRITE;
/*!40000 ALTER TABLE `tbclienteemail` DISABLE KEYS */;
INSERT INTO `tbclienteemail` VALUES (82,'dadda@awdwa.com',24,1),(83,'dadda@awdwa.com',25,1),(84,'awdwad@wadwa.dcom',35,1),(88,'caique.galvanin@gmail.com',18,1),(89,'caique.galvanin2@gmail.com',18,1),(90,'caique.rodrigues@hotmail.com.br',18,1),(91,'123123aweawd@awdaw.com',46,1),(92,'awdad2d2a@wadwad.com',46,1);
/*!40000 ALTER TABLE `tbclienteemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteendereco`
--

DROP TABLE IF EXISTS `tbclienteendereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteendereco` (
  `id_clienteendereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_endereco` varchar(255) DEFAULT NULL,
  `tx_bairro` varchar(100) DEFAULT NULL,
  `tx_uf` char(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cep` varchar(50) DEFAULT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `tx_observacao` text,
  `tx_complemento` varchar(100) DEFAULT NULL,
  `tx_cidade` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteendereco`),
  KEY `id_cliente` (`id_cliente`),
  KEY `tbclienteendereco_ibfk_2` (`id_tenant`),
  CONSTRAINT `tbclienteendereco_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclienteendereco_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteendereco`
--

LOCK TABLES `tbclienteendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteendereco` DISABLE KEYS */;
INSERT INTO `tbclienteendereco` VALUES (58,'Praça da Sé','Sé','SP','a','01001-000',35,'awdwad','lado ímpar','São Paulo',1),(61,'Rua Júlio Mori','Jardim Ouro Verde','SP','530','19906-000',18,'obs1','edicula','Ourinhos',1),(62,'Rua Washington Luis','Centro','SP','1500','18950-000',18,'ob2','Casa','Ipaussu',1),(63,NULL,NULL,NULL,NULL,NULL,18,NULL,NULL,NULL,1),(64,'Praça da Sé','Sé','SP','amdiao','01001-000',46,'awoidja','lado ímpar','São Paulo',1),(65,'eafeaf','eaf','SP','aef','18950-000',46,'eaf','eaf','Ipaussu',1);
/*!40000 ALTER TABLE `tbclienteendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressado`
--

DROP TABLE IF EXISTS `tbclienteinteressado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressado` (
  `id_clienteinteressado` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(255) NOT NULL,
  `dt_nascimento` date DEFAULT NULL,
  `st_tipopessoa` char(1) NOT NULL,
  `tx_cpf` varchar(14) DEFAULT NULL,
  `tx_cnpj` varchar(18) DEFAULT NULL,
  `tx_rg` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) DEFAULT NULL,
  `tx_sexo` char(1) DEFAULT NULL,
  `tx_observacao` text,
  `dt_primeirocontato` datetime DEFAULT NULL,
  `tx_origemcontato` varchar(255) DEFAULT NULL,
  `tx_objetivocontato` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressado_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressado`
--

LOCK TABLES `tbclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbclienteinteressado` VALUES (8,'Outro teste','1994-01-06','F','494.949.494-94',NULL,'12312x',1,'F','aaaa','2017-11-26 13:35:00',NULL,NULL),(9,'Outro teste','0000-00-00','F','494.949.494-94',NULL,'12312x',1,'F',NULL,'2017-11-25 11:30:00',NULL,NULL),(10,'Outro teste','1994-01-06','F','344.990.358-64',NULL,'12312x',1,'F',NULL,'2017-11-25 11:30:00',NULL,NULL),(11,'Outro teste','0000-00-00','F','494.949.494-94',NULL,'12312x',1,'F',NULL,'1970-01-01 00:00:00',NULL,NULL),(12,'awdwadwa',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadoemail`
--

DROP TABLE IF EXISTS `tbclienteinteressadoemail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadoemail` (
  `id_clientelinteressadoemail` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_email` varchar(100) NOT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clientelinteressadoemail`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadoemail_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadoemail_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadoemail`
--

LOCK TABLES `tbclienteinteressadoemail` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoemail` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadoemail` VALUES (8,'123123aweawd@awdaw.com',8,1),(9,'awdad2d2a@wadwad.com',8,1);
/*!40000 ALTER TABLE `tbclienteinteressadoemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadoendereco`
--

DROP TABLE IF EXISTS `tbclienteinteressadoendereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadoendereco` (
  `id_clienteinteressadoendereco` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_endereco` varchar(255) DEFAULT NULL,
  `tx_bairro` varchar(100) DEFAULT NULL,
  `tx_uf` char(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cep` varchar(50) DEFAULT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `tx_observacao` text,
  `tx_complemento` varchar(100) DEFAULT NULL,
  `tx_cidade` varchar(100) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteinteressadoendereco`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadoendereco_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadoendereco_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadoendereco`
--

LOCK TABLES `tbclienteinteressadoendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadoendereco` VALUES (8,'Praça da Sé','Sé','SP','amdiao','01001-000',8,'awoidja','lado ímpar','São Paulo',1),(9,'eafeaf','eaf','SP','aef','18950-000',8,'eaf','eaf','Ipaussu',1);
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclienteinteressadotelefone`
--

DROP TABLE IF EXISTS `tbclienteinteressadotelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclienteinteressadotelefone` (
  `id_clienteinteressadotelefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_telefone` varchar(100) NOT NULL,
  `tx_tipo` varchar(100) NOT NULL,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clienteinteressadotelefone`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclienteinteressadotelefone_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbclienteinteressadotelefone_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclienteinteressadotelefone`
--

LOCK TABLES `tbclienteinteressadotelefone` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` DISABLE KEYS */;
INSERT INTO `tbclienteinteressadotelefone` VALUES (2,'(12) 31231-23','fixo_so',9,1),(4,'(12) 31231-23','fixo_so',11,1),(9,'(12) 31231-23','fixo_so',10,1),(18,'(12) 31231-23','fixo_so',8,1);
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbclientetelefone`
--

DROP TABLE IF EXISTS `tbclientetelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbclientetelefone` (
  `id_clientetelefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_telefone` varchar(100) NOT NULL,
  `tx_tipo` varchar(100) NOT NULL,
  `id_cliente` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  PRIMARY KEY (`id_clientetelefone`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbclientetelefone_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `tbcliente` (`id_cliente`),
  CONSTRAINT `tbclientetelefone_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbclientetelefone`
--

LOCK TABLES `tbclientetelefone` WRITE;
/*!40000 ALTER TABLE `tbclientetelefone` DISABLE KEYS */;
INSERT INTO `tbclientetelefone` VALUES (63,'a111111','fixo_so',35,1),(70,'(14) 33441-447','fixo_so',18,1),(71,'(14) 99760-6649','fixo_so',18,1),(72,'(12) 31231-23','fixo_so',46,1);
/*!40000 ALTER TABLE `tbclientetelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinteracaoclienteinteressado`
--

DROP TABLE IF EXISTS `tbinteracaoclienteinteressado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinteracaoclienteinteressado` (
  `id_interacaoclienteinteressado` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_clienteinteressado` bigint(20) NOT NULL,
  `tx_interacao` text NOT NULL,
  `dt_datahora` datetime NOT NULL,
  `tx_tipo` varchar(255) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `id_usuariotenant_criadopor` bigint(20) NOT NULL,
  `id_usuariotenant_responsavelpor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_interacaoclienteinteressado`),
  KEY `id_clienteinteressado` (`id_clienteinteressado`),
  KEY `id_tenant` (`id_tenant`),
  KEY `tbinteracaoclienteinteressado_ibfk_4` (`id_usuariotenant_criadopor`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_1` FOREIGN KEY (`id_clienteinteressado`) REFERENCES `tbclienteinteressado` (`id_clienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_3` FOREIGN KEY (`id_usuariotenant_criadopor`) REFERENCES `tbusuariotenant` (`id_usuariotenant`),
  CONSTRAINT `tbinteracaoclienteinteressado_ibfk_4` FOREIGN KEY (`id_usuariotenant_criadopor`) REFERENCES `tbusuariotenant` (`id_usuariotenant`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinteracaoclienteinteressado`
--

LOCK TABLES `tbinteracaoclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbinteracaoclienteinteressado` VALUES (44,8,'teste','2017-12-10 19:22:00','ligar',1,2,2),(45,8,'teste','2017-12-19 20:16:12','ligar',1,2,2);
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbinteracaoclienteinteressadoanexo`
--

DROP TABLE IF EXISTS `tbinteracaoclienteinteressadoanexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbinteracaoclienteinteressadoanexo` (
  `id_interacaoclienteinteressadoanexo` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_folder` varchar(500) NOT NULL,
  `tx_nomeservidor` varchar(500) NOT NULL,
  `id_interacaoclienteinteressado` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `tx_nomearquivo` varchar(500) NOT NULL,
  PRIMARY KEY (`id_interacaoclienteinteressadoanexo`),
  KEY `id_tenant` (`id_tenant`),
  KEY `tbclienteinteressadoanexo_ibfk_1_idx` (`id_interacaoclienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressadoanexo_ibfk_1` FOREIGN KEY (`id_interacaoclienteinteressado`) REFERENCES `tbinteracaoclienteinteressado` (`id_interacaoclienteinteressado`),
  CONSTRAINT `tbinteracaoclienteinteressadoanexo_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbinteracaoclienteinteressadoanexo`
--

LOCK TABLES `tbinteracaoclienteinteressadoanexo` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` DISABLE KEYS */;
INSERT INTO `tbinteracaoclienteinteressadoanexo` VALUES (8,'uploads/crm-interacao/anexos/c4ca4238a0b923820dcc509a6f75849b','5a2db22c4d3323.02714097.jpg',45,1,'download.jpg');
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpermissao`
--

DROP TABLE IF EXISTS `tbpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpermissao` (
  `id_permissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_panel` varchar(255) NOT NULL,
  `tx_nomepermissao` varchar(100) NOT NULL,
  `tx_controller` varchar(100) NOT NULL,
  `tx_tipopermissao` varchar(10) NOT NULL,
  `tx_identificadorpermissao` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpermissao`
--

LOCK TABLES `tbpermissao` WRITE;
/*!40000 ALTER TABLE `tbpermissao` DISABLE KEYS */;
INSERT INTO `tbpermissao` VALUES (1,'Usuários','Cadastrar/Alterar Usuários','usuario','I|A',''),(2,'Usuários','Excluir Usuário','usuario','E',''),(3,'Usuários','Somente Visualizar','usuario','V',''),(4,'Cliente','Cadastrar/Alterar Clientes','cliente','I|A',''),(5,'Cliente','Excluir Cliente','cliente','E',''),(6,'Cliente','Somente Visualizar','cliente','V',''),(7,'Usuários','Alterar Permissões dos Usuários','usuario','R','alterar-permissoes-usuarios'),(10,'Cadastro de Interessados','Cadastro de Interessados','crm-interessado','I|A',NULL),(11,'Cadastro de Interessados','Excluir Cadastro de Interessados','crm-interessado','E',NULL),(12,'Cadastro de Interessados','Somente Visualizar','crm-interessado','V',NULL),(13,'Cadastro de Interessados','Cadastrar Interação','crm-interessado','I|A','cadastrar-interacao-crm-interessado'),(14,'Cadastro de Interessados','Excluir Interação','crm-interessado','E','excluir-interacao-crm-interessado'),(16,'Cadastro de Interessados','Somente Visualizar Interação','crm-interessado','V','visualizar-interacao-crm-interessado');
/*!40000 ALTER TABLE `tbpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtenant`
--

DROP TABLE IF EXISTS `tbtenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtenant` (
  `id_tenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomefantasia` varchar(255) NOT NULL,
  `tx_cnpj` varchar(18) NOT NULL,
  PRIMARY KEY (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtenant`
--

LOCK TABLES `tbtenant` WRITE;
/*!40000 ALTER TABLE `tbtenant` DISABLE KEYS */;
INSERT INTO `tbtenant` VALUES (1,'Usuario Teste','24.381.697/0001-56');
/*!40000 ALTER TABLE `tbtenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenant`
--

DROP TABLE IF EXISTS `tbusuariotenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenant` (
  `id_usuariotenant` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nomeusuario` varchar(255) NOT NULL,
  `tx_email` varchar(255) DEFAULT NULL,
  `tx_login` varchar(150) NOT NULL,
  `tx_senha` varchar(100) DEFAULT NULL,
  `fb_userid` varchar(200) DEFAULT NULL,
  `st_tipologin` char(1) DEFAULT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `st_ativo` char(1) NOT NULL DEFAULT 'S',
  `st_admin` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenant_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenant`
--

LOCK TABLES `tbusuariotenant` WRITE;
/*!40000 ALTER TABLE `tbusuariotenant` DISABLE KEYS */;
INSERT INTO `tbusuariotenant` VALUES (2,'Caique Galvanin Rodrigues 2','caique.galvanin@gmail.com','caique','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S','S'),(3,'a','a@a.com.br','123','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,NULL,1,'S','N');
/*!40000 ALTER TABLE `tbusuariotenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuariotenantpermissao`
--

DROP TABLE IF EXISTS `tbusuariotenantpermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuariotenantpermissao` (
  `id_usuariotenantpermissao` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuariotenant` bigint(20) NOT NULL,
  `id_tenant` bigint(20) NOT NULL,
  `id_permissao` bigint(20) NOT NULL,
  PRIMARY KEY (`id_usuariotenantpermissao`),
  KEY `id_usuariotenant` (`id_usuariotenant`),
  KEY `id_tenant` (`id_tenant`),
  KEY `id_permissao` (`id_permissao`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_1` FOREIGN KEY (`id_usuariotenant`) REFERENCES `tbusuariotenant` (`id_usuariotenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_2` FOREIGN KEY (`id_tenant`) REFERENCES `tbtenant` (`id_tenant`),
  CONSTRAINT `tbusuariotenantpermissao_ibfk_3` FOREIGN KEY (`id_permissao`) REFERENCES `tbpermissao` (`id_permissao`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuariotenantpermissao`
--

LOCK TABLES `tbusuariotenantpermissao` WRITE;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` DISABLE KEYS */;
INSERT INTO `tbusuariotenantpermissao` VALUES (1,3,1,10),(2,3,1,11),(3,3,1,16);
/*!40000 ALTER TABLE `tbusuariotenantpermissao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-10 20:18:17
