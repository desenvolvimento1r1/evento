-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: evento
-- ------------------------------------------------------
-- Server version	5.7.19-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `tbbancoconta`
--

LOCK TABLES `tbbancoconta` WRITE;
/*!40000 ALTER TABLE `tbbancoconta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbbancoconta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbcategoriaconta`
--

LOCK TABLES `tbcategoriaconta` WRITE;
/*!40000 ALTER TABLE `tbcategoriaconta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcategoriaconta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbcliente`
--

LOCK TABLES `tbcliente` WRITE;
/*!40000 ALTER TABLE `tbcliente` DISABLE KEYS */;
INSERT INTO `tbcliente` VALUES (5,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,4),(6,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,5),(7,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,6);
/*!40000 ALTER TABLE `tbcliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteemail`
--

LOCK TABLES `tbclienteemail` WRITE;
/*!40000 ALTER TABLE `tbclienteemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclienteemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteendereco`
--

LOCK TABLES `tbclienteendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteendereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclienteendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteinteressado`
--

LOCK TABLES `tbclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbclienteinteressado` VALUES (3,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,'N'),(4,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,5,'N'),(5,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,6,'N'),(6,'teste interessado',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,7,'S');
/*!40000 ALTER TABLE `tbclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteinteressadoemail`
--

LOCK TABLES `tbclienteinteressadoemail` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclienteinteressadoemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteinteressadoendereco`
--

LOCK TABLES `tbclienteinteressadoendereco` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclienteinteressadoendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclienteinteressadotelefone`
--

LOCK TABLES `tbclienteinteressadotelefone` WRITE;
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclienteinteressadotelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbclientetelefone`
--

LOCK TABLES `tbclientetelefone` WRITE;
/*!40000 ALTER TABLE `tbclientetelefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbclientetelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbcontareceber`
--

LOCK TABLES `tbcontareceber` WRITE;
/*!40000 ALTER TABLE `tbcontareceber` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcontareceber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbcontareceberanexo`
--

LOCK TABLES `tbcontareceberanexo` WRITE;
/*!40000 ALTER TABLE `tbcontareceberanexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbcontareceberanexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionario`
--

LOCK TABLES `tbfuncionario` WRITE;
/*!40000 ALTER TABLE `tbfuncionario` DISABLE KEYS */;
INSERT INTO `tbfuncionario` VALUES (12,'aasmdpok',NULL,'F',NULL,NULL,NULL,1,NULL,NULL,'itau','0552','13718-4',1500.00);
/*!40000 ALTER TABLE `tbfuncionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionarioafastamento`
--

LOCK TABLES `tbfuncionarioafastamento` WRITE;
/*!40000 ALTER TABLE `tbfuncionarioafastamento` DISABLE KEYS */;
INSERT INTO `tbfuncionarioafastamento` VALUES (7,12,'2018-07-25','2018-07-29','doença',1),(8,12,'2018-08-09','2018-08-11','morte',1);
/*!40000 ALTER TABLE `tbfuncionarioafastamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionariodependente`
--

LOCK TABLES `tbfuncionariodependente` WRITE;
/*!40000 ALTER TABLE `tbfuncionariodependente` DISABLE KEYS */;
INSERT INTO `tbfuncionariodependente` VALUES (22,12,'a','I','2018-06-30',1),(23,12,'c','O','1994-01-06',1);
/*!40000 ALTER TABLE `tbfuncionariodependente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionarioemail`
--

LOCK TABLES `tbfuncionarioemail` WRITE;
/*!40000 ALTER TABLE `tbfuncionarioemail` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbfuncionarioemail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionarioendereco`
--

LOCK TABLES `tbfuncionarioendereco` WRITE;
/*!40000 ALTER TABLE `tbfuncionarioendereco` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbfuncionarioendereco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionarioferias`
--

LOCK TABLES `tbfuncionarioferias` WRITE;
/*!40000 ALTER TABLE `tbfuncionarioferias` DISABLE KEYS */;
INSERT INTO `tbfuncionarioferias` VALUES (20,12,'2018-07-09','2018-07-31',1),(21,12,'2018-07-01','2018-07-31',1),(22,12,'2018-08-01','2018-08-11',1);
/*!40000 ALTER TABLE `tbfuncionarioferias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbfuncionariotelefone`
--

LOCK TABLES `tbfuncionariotelefone` WRITE;
/*!40000 ALTER TABLE `tbfuncionariotelefone` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbfuncionariotelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbinteracaoclienteinteressado`
--

LOCK TABLES `tbinteracaoclienteinteressado` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` DISABLE KEYS */;
INSERT INTO `tbinteracaoclienteinteressado` VALUES (7,4,'teste','2018-06-27 11:30:05','almoco',1,2,2),(8,5,'teste 2','2018-06-23 11:33:40','ligar',1,2,2),(9,6,'aaa','2018-06-29 11:40:52','email',1,2,2),(10,4,'a','2018-06-27 10:36:09','almoco',1,2,2);
/*!40000 ALTER TABLE `tbinteracaoclienteinteressado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbinteracaoclienteinteressadoanexo`
--

LOCK TABLES `tbinteracaoclienteinteressadoanexo` WRITE;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbinteracaoclienteinteressadoanexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbpermissao`
--

LOCK TABLES `tbpermissao` WRITE;
/*!40000 ALTER TABLE `tbpermissao` DISABLE KEYS */;
INSERT INTO `tbpermissao` VALUES (1,'Usuários','Cadastrar/Alterar Usuários','usuario','I|A',''),(2,'Usuários','Excluir Usuário','usuario','E',''),(3,'Usuários','Somente Visualizar','usuario','V',''),(4,'Cliente','Cadastrar/Alterar Clientes','cliente','I|A',''),(5,'Cliente','Excluir Cliente','cliente','E',''),(6,'Cliente','Somente Visualizar','cliente','V',''),(7,'Usuários','Alterar Permissões dos Usuários','usuario','R','alterar-permissoes-usuarios'),(10,'Cadastro de Interessados','Cadastro de Interessados','crm-interessado','I|A',NULL),(11,'Cadastro de Interessados','Excluir Cadastro de Interessados','crm-interessado','E',NULL),(12,'Cadastro de Interessados','Somente Visualizar','crm-interessado','V',NULL),(13,'Cadastro de Interessados','Cadastrar Interação','crm-interessado','I|A','cadastrar-interacao-crm-interessado'),(14,'Cadastro de Interessados','Excluir Interação','crm-interessado','E','excluir-interacao-crm-interessado'),(16,'Cadastro de Interessados','Somente Visualizar Interação','crm-interessado','V','visualizar-interacao-crm-interessado');
/*!40000 ALTER TABLE `tbpermissao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbtenant`
--

LOCK TABLES `tbtenant` WRITE;
/*!40000 ALTER TABLE `tbtenant` DISABLE KEYS */;
INSERT INTO `tbtenant` VALUES (1,'Usuario Teste','24.381.697/0001-56');
/*!40000 ALTER TABLE `tbtenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbusuario`
--

LOCK TABLES `tbusuario` WRITE;
/*!40000 ALTER TABLE `tbusuario` DISABLE KEYS */;
INSERT INTO `tbusuario` VALUES (1,'Caique Galvanin','caique.galvanin@gmail.com','caique','123',NULL,''),(2,'HudFastswitch','','',NULL,'100810150678770','F');
/*!40000 ALTER TABLE `tbusuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbusuariotenant`
--

LOCK TABLES `tbusuariotenant` WRITE;
/*!40000 ALTER TABLE `tbusuariotenant` DISABLE KEYS */;
INSERT INTO `tbusuariotenant` VALUES (2,'Caique Galvanin Rodrigues 2','caique.galvanin@gmail.com','caique','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,'',1,'S','S'),(3,'a','a@a.com.br','123','40bd001563085fc35165329ea1ff5c5ecbdbbeef',NULL,NULL,1,'S','N');
/*!40000 ALTER TABLE `tbusuariotenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tbusuariotenantpermissao`
--

LOCK TABLES `tbusuariotenantpermissao` WRITE;
/*!40000 ALTER TABLE `tbusuariotenantpermissao` DISABLE KEYS */;
INSERT INTO `tbusuariotenantpermissao` VALUES (1,3,1,10),(2,3,1,11),(3,3,1,16);
/*!40000 ALTER TABLE `tbusuariotenantpermissao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-01 17:22:00
