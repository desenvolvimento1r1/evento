<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Zend\Config;

use Exception;
use FluentPDO;
use PDO;
use Zend\Db\Adapter\AdapterInterface;

/**
 *
 * @property AdapterInterface $adapter
 * @property int $lastInsertValue
 * @property string $table
 */
abstract class CustomAbstractTableGateway {

    protected $table = null;
    protected $_primary = null;
    public $primaryKey = '';

    public function findOneById($primary_key, $columns = []) {
        $db = (new \Base\Service\Connection)->getConnection();
        $query = $db->from($this->table)
                ->where("$this->_primary = {$primary_key}")
                ->limit(1);

        $result = $query->fetch();

        if (empty($result)) {
            $arrReturn = [];
        } else {
            $arrReturn = $result;
            if (!empty($columns)) {
                $arrReturn = [];
                foreach ($columns as $column) {
                    $arrReturn[$column] = $result[$column];
                }
            }
        }
        return $arrReturn;
    }

    public function save($data, $adapter) {
        try {
            $db = $adapter;
//            $data = array_filter($data);
//            $data = array_map(function($v) {
//                return (!isset($v)) ? null : $v;
//            }, $data);
//            if (!empty($data)) {
            foreach ($data as $key => $d) {
                if (trim($d) == '') {
                    $data[$key] = null;
                }
            }
//            $data = array_filter($data);
//            }

            if (!empty($data[$this->_primary])) {
                $primary_key = $data[$this->_primary];
            } else {
                $primary_key = null;
            }

            $id_tenant = (new \Zend\Session\Container('admin'))->id_tenant;

            if (empty($id_tenant)) {
                die('tenant não reconhecido. die por questão de segurança');
            }

            $data['id_tenant'] = $id_tenant;

            if (empty($primary_key)) {
//                unset($data[$this->_primary]);
                $data = array_filter($data);
                $id = $db->insertInto($this->table, $data)->execute();

                return $id;
            } else {
                if ($this->findOneById($primary_key)) {

                    $db->update($this->table, $data)->where($this->_primary . " = '{$primary_key}'")->execute();

                    return $primary_key;
                } else {
                    throw new Exception("Registro não encontrado");
                }
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }

    public function removeAll($where, $adapter) {

        $db = $adapter;
        try {

            $id_tenant = (new \Zend\Session\Container('admin'))->id_tenant;

            if (empty($id_tenant)) {
                die('tenant não reconhecido. die por questão de segurança');
            }

            $query = $db->deleteFrom($this->table)
                    ->where("id_tenant = '{$id_tenant}'");

            if (is_array($where)) {
                foreach ($where as $column => $value) {
                    $query->where("{$column} = '{$value}'");
                }
            } else {
                $query->where($where);
            }
            $query->execute();


            return true;
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }

        return false;
    }

}
