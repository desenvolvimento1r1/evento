<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Zend\Mvc\Controller;

use Base\Service\Tbusuariotenantpermissao;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;

/**
 * Basic action controller
 */
abstract class CustomActionController extends AbstractActionController {

    public $module;
    public $controller;
    public $tituloPagina = "";
    public $subTituloPagina = "";
    public $possuiPermissaoCadastrar;
    public $possuiPermissaoExcluir;
    public $possuiPermissaoVisualizar;

    public function onDispatch(MvcEvent $e) {
        $adminContainer = new Container('admin');

        if (empty($adminContainer->getArrayCopy())) {
//se não encontrou a sessão do admin, é pq não fez login
//renderizo o layout do login para o usuario fazer login
            $this->module = 'login';
        }

        $possuiPermissao = $this->possuiPermissaoAcesso();

        $this->possuiPermissaoCadastrar = (new Tbusuariotenantpermissao())->possuiPermissaoCadastrar($this->controller);
        $this->possuiPermissaoExcluir = (new Tbusuariotenantpermissao())->possuiPermissaoExcluir($this->controller);
        $this->possuiPermissaoVisualizar = (new Tbusuariotenantpermissao())->possuiPermissaoVisualizar($this->controller);

        $content_only = '';
        if (!empty($_GET['content_only'])) {
            $content_only = $_GET['content_only'];
        }

        $response = parent::onDispatch($e);
        $this->layout()->setTemplate('layout/' . $this->module);
        $this->layout()->setVariables(array(
            'tituloPagina' => $this->tituloPagina,
            'subTituloPagina' => $this->subTituloPagina,
            'controller' => $this->controller,
            'possuiPermissao' => $possuiPermissao,
            'possuiPermissaoCadastrar' => $this->possuiPermissaoCadastrar,
            'possuiPermissaoExcluir' => $this->possuiPermissaoExcluir,
            'possuiPermissaoVisualizar' => $this->possuiPermissaoVisualizar,
            'content_only' => $content_only
        ));
        return $response;
    }

    public function possuiPermissaoAcesso() {

        $isAdmin = (new \Base\Service\Tbusuariotenant())->isAdmin();
        if ($this->controller == 'inicio' || $isAdmin) {
//todos usuarios possuem acesso ao inicio, ou se for admin (admin tem acesso ao sistema inteiro
            return true;
        }

        $Tbusuariopermissao = new Tbusuariotenantpermissao();

        $usuarioLogado = (new Container('usuarioLogado'))->getArrayCopy();

        if (!empty($usuarioLogado)) {
            $id_usuariotenant = $usuarioLogado['id_usuariotenant'];
            $permissaoController = $Tbusuariopermissao->fetchAll(array('id_usuariotenant' => $id_usuariotenant, 'tx_controller' => $this->controller));

            if (empty($permissaoController)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function DeleteCommomReturn() {
        return array(
            'type' => 'success',
            'title' => 'SUCESSO',
            'message' => 'Excluído com sucesso.'
        );
    }

    public function ExceptionCommonReturn($message) {
        $message = $this->tratativaMensagem($message);
        return array(
            'type' => 'error',
            'title' => 'ATENÇÃO',
            'message' => $message
        );
    }

    public function SalvarCommomReturn() {
        return array(
            'type' => 'success',
            'title' => 'SUCESSO',
            'message' => 'Salvo com sucesso.'
        );
    }

    public function tratativaMensagem($message) {
        if (stristr($message, 'SQLSTATE[23000]: Integrity constraint violation: 1451 Cannot delete or update a parent row: a foreign key constraint fails')) {
            return "Não foi possível remover o registro pois o mesmo possui vínculos no sistema";
        }
        return $message;
    }

}
