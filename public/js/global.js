var urlAtual = window.location.href;
var urlAtual = getUrlAtual(urlAtual);
var baseUrl = window.location.host;
var protocol = window.location.protocol;
if (!urlAtual.split('/')[4]) {
    controllerAtual = 'index';
} else {
    var controllerAtual = urlAtual.split('/')[4];
}
url = urlAtual;
urlPura = url.replace(/#[^#]*$/, "").replace(/\?[^\?]*$/, "").replace(/^https:/, "http:");
var moduloAtual = urlAtual.split('/')[3];
if (controllerAtual.indexOf('content_only') != -1) {
    controllerAtual = controllerAtual.split('?')[0];
} else {
    controllerAtual = urlAtual.split('/')[4];
}

var content_only = "";
if (urlAtual.indexOf('content_only') != -1) {
    content_only = "?content_only=true";
}


var _erroPadraoAjax = 'Ocorreu ao tentar executar a ação, entre em contato com a equipe técnica.';
var moduloAtual = urlAtual.split('/')[3];
function getUrlAtual(urlAtual) {
    if (urlAtual.indexOf('?') > 0) {
        urlAtual = urlAtual.split('?');
        if (urlAtual[1].indexOf('/') > 0) {
            param = urlAtual[1].split('/');
            idParam = param[1];
            urlAtual = urlAtual[0] + '/' + idParam + '?' + param[0];
        } else {
            urlAtual = urlAtual[0] + '?' + urlAtual[1];
        }
    }
    return urlAtual;
}


var urlAtual = window.location.href;
var urlAtual = getUrlAtual(urlAtual);
var baseUrl = window.location.host;
var protocol = window.location.protocol;
var htmlMsgAguarde = "";
htmlMsgAguarde += "";
htmlMsgAguarde += "    <!-- Modal da mensagem do aguarde -->";
htmlMsgAguarde += "    <div class=\"modal fade modalSistema\" id=\"modalMensagem\" tabindex=\"-1\" role=\"aguarde\" aria-labelledby=\"aguarde\" style=\"z-index:9999;position: absolute;  top: 50%;left: 50%;transform: translate(-50%, -50%);\">";
htmlMsgAguarde += "        <div class=\"modal-dialog\" role=\"document\" style=\"width:85px\">";
htmlMsgAguarde += "            <div class=\"modal-content\" style=\"width:85px\">";
htmlMsgAguarde += "                <div class=\"modal-body\" style=\"font-size: 32px;font-weight: bold; text-align: center\">";
htmlMsgAguarde += "                    <div class=\"lds-css ng-scope\">";
htmlMsgAguarde += "                    <div style=\"width:100%;height:100%\" class=\"lds-dual-ring\">";
htmlMsgAguarde += "                <div><\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "                <\/div>";
htmlMsgAguarde += "            <\/div>";
htmlMsgAguarde += "        <\/div>";
htmlMsgAguarde += "    <\/div>";
function OpenMsgAguarde() {
    $("#div-modalMensagem").html(htmlMsgAguarde);
    $("#modalMensagem").modal();
}

function CloseMsgAguarde() {
    $("#modalMensagem").modal("hide");
    $(".modal-backdrop").remove();
    $("body").removeAttr("style");
}

/**
 * @param {string} tipo:    - info <br />
 *                          - warning <br />
 *                          - success <br />
 *                          - error <br />
 *                          - question <br />
 *                          
 * @param {bool} exibirButtonConfirm: exibe ou não o botão no final da mensagem, texto padrão 'OK'
 * @param {string} tituloBotaoConfirm: mensagem do texto do botão de confirmação, padrão: 'OK'
 */
function openMsg(titulo = "", mensagem = "", tipo = "info", exibirButtonConfirm = true, tituloBotaoConfirm = 'OK') {
    swal({
        title: titulo,
        text: mensagem,
        type: tipo,
        showConfirmButton: exibirButtonConfirm,
        confirmButtonText: tituloBotaoConfirm
    });
}

/**
 * @param {string} tipo:    - info <br />
 *                          - warning <br />
 *                          - success <br />
 *                          - error <br />
 *                          - question <br />
 */
function removeData(titulo = "", mensagem = "", tipo = "info", objRemoveTable = "", url) {
    swal({
        title: titulo,
        text: mensagem,
        type: tipo,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim',
        cancelButtonText: 'Não'
    }).then(function (result) {
        if (result.value) {
            OpenMsgAguarde();
            $.ajax({
                url: url,
                dataType: 'json',
                success: function (r) {
                    if (r.type == 'success') {
                        title = 'Sucesso';
                        $(objRemoveTable).closest('tr').remove();
                    } else {
                        title = 'Ops';
                    }
                    swal({
                        title: title,
                        text: r.message,
                        type: r.type
                    });
                },
                complete: function () {
                    CloseMsgAguarde();
                }
            });
        }
    })
}


function _initDateRangePicker() {
    $(".daterange").daterangepicker({
        locale: {
            format: 'DD/MM/YYYY',
            locale: 'pt-br',
            "applyLabel": "Aplicar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Personalizado",
        },
    });
}

function _initDatePicker() {
    $('.date').datetimepicker({
        locale: 'pt-br',
        format: 'DD/MM/YYYY'
    });
}
function _initDateTimePicker() {
    $('.datetime').datetimepicker({
        locale: 'pt-br',
        format: 'DD/MM/YYYY HH:mm:ss'
    });
}

function formDefault(formIdentification, urlRedirectSuccess) {
//    if ($(formIdentification).valid()) {
    OpenMsgAguarde();
    $(formIdentification).ajaxSubmit({
        type: 'POST',
        dataType: 'json',
        success: function (r) {
            CloseMsgAguarde();
            if (r.type === 'success') {
                openMsg('Sucesso', r.message, 'success', true, 'Fechar');
                swal.showLoading();
                if (!empty(urlRedirectSuccess)) {
                    setTimeout(function () {
                        window.location = urlRedirectSuccess;
                    }, 2000);
                }
            } else {
                openMsg('Ops', r.message, 'error', true, 'Fechar');
            }
        },
        error: function (r) {
            if (empty(r.message)) {
                r.message = _erroPadraoAjax;
            }
            openMsg('Ops', r.message, 'error', true, 'Fechar');
        }
    });
//    } else {
//        openMsg('Ops', 'Alguns campos não foram preenchidos corretamente, verifique novamente', 'warning', true, 'Fechar');
//        return false;
//    }
}

function formDefaultSave(formIdentification) {
    OpenMsgAguarde();
    $(formIdentification).ajaxSubmit({
        type: 'POST',
        dataType: 'json',
        success: function (r) {
            CloseMsgAguarde();
            if (r.type === 'success') {
                openMsg('Sucesso', r.message, 'success', true, 'Fechar');
                if (urlAtual.toString().indexOf('?id=') < 1) {
                    swal.showLoading();
                }
                setTimeout(function () {
                    if (urlAtual.toString().indexOf('?id=') < 1) {
                        window.location = urlAtual + '?id=' + r.id;
                    }

                }, 2000);
            } else {
                openMsg('Ops', r.message, 'error', true, 'Fechar');
            }
        },
        error: function (r) {
            if (empty(r.message)) {
                r.message = _erroPadraoAjax;
            }
            CloseMsgAguarde();
            openMsg('Ops', r.message, 'error', true, 'Fechar');
        }
    });
}
function _initMask() {
    $('.placeholder').unmask();
    $('.date').unmask();
    $('.time').unmask();
    $('.datetime').unmask();
    $('.cep').unmask();
    $('.cpf').unmask();
    $('.money').unmask();
    $('.percent').unmask();
    $('.telefone').unmask();
    $('.cnpj').unmask();
    $('.rg').unmask();
    $('.agencia').unmask();
    $('.conta').unmask();
    $('.year').unmask();
    $('.placeholder').mask("99/99/9999", {placeholder: "__/__/____"});
    $('.date').mask('99/99/9999');
    $('.year').mask('9999');
    $('.time').mask('99:99:99');
    $('.datetime').mask('99/99/9999 99:99:99');
    $('.cep').mask('99999-999');
    $('.cpf').mask('999.999.999-99', {reverse: true});
    $(".cnpj").mask("99.999.999/9999-99"); // Máscara para CNPJ
    $('.rg').mask('99.999.999-9'); // Máscara para RG
    $('.agencia').mask('9999-9'); // Máscara para AGÊNCIA BANCÁRIA
    $('.conta').mask('99.999-9'); // Máscara para CONTA BANCÁRIA
    $(".money").maskMoney({decimal: ",", thousands: ".", precision: 2, allowZero: true});
    $(".decimal").maskMoney({decimal: ",", thousands: ".", precision: 2, allowZero: true});
    $(".decimal4").maskMoney({decimal: ",", thousands: ".", precision: 4});
    $(".percent").maskMoney({decimal: ",", thousands: ".", precision: 2});
    $(".telefone").mask("(99) 9999?9-9999");
    $(".telefone").on("blur", function () { // verifica se é tel fixo ou celular
        var last = $(this).val().substr($(this).val().indexOf("-") + 1);
        if (last.length == 3) {
            var move = $(this).val().substr($(this).val().indexOf("-") - 1, 1);
            var lastfour = move + last;
            var first = $(this).val().substr(0, 9);
            $(this).val(first + '-' + lastfour);
        }
    });
    //PERMITE SOMENTE QUE DIGITE LETRAS NO CAMPOS
    $('.alphaonly').bind('keyup input', function ()
    {
        if (this.value.match(/[^a-zA-Z áéíóúÁÉÍÓÚüÜ]/g))
        {
            this.value = this.value.replace(/[^a-zA-Z áéíóúÁÉÍÓÚüÜ]/g, '');
        }
    });
    //PERMITE SOMENTE QUE DIGITE LETRAS NO CAMPOS
    $('.numericonly').bind('keyup input', function ()
    {
        if (this.value.match(/[^0-1]/g))
        {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });
//    $(".labelcolor").click(function () {
//        if ($(this).find('input:checkbox:first').is(':checked')) {
//            $(this).addClass('bgYellow');
//        } else {
//            $(this).removeClass('bgYellow');
//        }
//    });
}

function _initToolTip() {
    $('[data-toggle="tooltip"], .tooltip').tooltip();
}

function __initDataTable(identificador) {
    $(identificador).DataTable({
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        "lengthMenu": [[25, 50, 100, -1], [25, 50, 100, "Tudo"]],
        "language": {
            "oAria": {
                "sSortAscending": " - clique para ordenar ascendente",
                "sSortDescending": " - clique para ordenar descendente"
            },
            "oPaginate": {
                "sFirst": "Primeira página",
                "sPrevious": "Página anterior",
                "sNext": "Próxima página",
                "sLast": "Última página"
            },
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Exibindo de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Nenhum registro para exibir",
            "sInfoFiltered": " (filtrado de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Exibir _MENU_ registros por página",
            "sLoadingRecords": "Por favor aguarde - carregando...",
            "sProcessing": "Processando...",
            "sSearch": "Filtrar:",
            "sUrl": "",
            "sZeroRecords": "Nenhum registro para exibir"
        }
    })
}


function formValidate(formIdentificator) {
    $(formIdentificator).validetta({
        realTime: true,
        onValid: function (event) {
            event.preventDefault(); // if you dont break submit and if form doesnt have error, page will post
            formDefaultSave("#validate");
        },
        onError: function (event) {
            openMsg('Ops', 'Alguns campos não foram preenchidos corretamente, verifique novamente', 'warning', true, 'Fechar');
            return false;
        }
    });
}


/**
 * @params {string} tipo = - ''(vazio) para default <br/>
 *                          - modal-primary<br/>
 *                          - modal-info<br/>
 *                          - modal-warning<br/>
 *                          - modal-success<br/>
 *                          - modal-danger<br/>
 *         {string} title = titulo da modal<br/>
 *         {string} content = conteudo da modal
 */
function _globalModal(tipo = '', title = '', content = '') {
    var globalModal = '<div class="modal ' + tipo + ' fade" id="global-modal">' +
            '    <div class="modal-dialog">' +
            '        <div class="modal-content">' +
            '            <div class="modal-header">' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '                    <span aria-hidden="true">×</span></button>' +
            '                <h4 class="modal-title">Default Modal</h4>' +
            '            </div>' +
            '            <div class="modal-body">' +
            '                <p>One fine body…</p>' +
            '            </div>' +
            '            <div class="modal-footer">' +
            '                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>' +
//            '                <button type="button" class="btn btn-primary">Save changes</button>' +
            '            </div>' +
            '        </div>' +
            '        <!-- /.modal-content -->' +
            '    </div>' +
            '    <!-- /.modal-dialog -->' +
            '</div>';
    $("#global-modal").html(globalModal);
    $("#global-modal").modal();
}

function moneyToFloat($vl) {
    if ($.trim($vl) == '') {
        return 0;
    } else {
        $vl = str_replace('.', '', $vl);
        $vl = str_replace(',', '.', $vl);
        return parseFloat($vl);
    }
}

function floatToMoney($vl, $casasDecimais) {
    if ($.trim($casasDecimais) == '') {
        $casasDecimais = 2;
    }
    return number_format($vl, $casasDecimais, ',', '.');
}

function reverseDate($dataParam) {
//
    $horas = '';
    $dateConverted = $dataParam;
    if (strstr($dataParam, " ")) {
        $dataParam = explode(" ", $dataParam);
        $horas = $dataParam[1];
        $dataParam = $dataParam[0];
    }
    if (strstr($dataParam, '-')) {
        $dateConverted = date('d/m/Y', strtotime($dataParam));
    } else if (strstr($dataParam, '/')) {
        $dateConverted = implode('-', array_reverse(explode('/', ($dataParam))));
    }

    if (!empty($horas)) {
        $dateConverted = $dateConverted + " " + $horas;
    }
    return $dateConverted;
}