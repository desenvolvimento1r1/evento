﻿/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

        CKEDITOR.editorConfig = function(config) {

        // %REMOVE_START%
        // The configuration options below are needed when running CKEditor from source files.
        config.plugins = 'dialogui,dialog,about,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,notification,button,toolbar,clipboard,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,copyformatting,resize,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,floatingspace,listblock,richcombo,font,format,horizontalrule,htmlwriter,iframe,wysiwygarea,image,indent,indentblock,indentlist,smiley,justify,menubutton,link,list,liststyle,magicline,maximize,newpage,pagebreak,pastetext,pastefromword,preview,print,removeformat,save,selectall,showblocks,showborders,sourcearea,specialchar,scayt,stylescombo,tab,table,tabletools,tableselection,undo,wsc,simage,lineutils,widgetselection,widget,filetools,notificationaggregator,uploadwidget,uploadimage,imageuploader';
                config.skin = 'moono-lisa';
                config.uploadUrl = '/uploader/upload.php'; // %REMOVE_END%

                // Define changes to default configuration here. For example:
                // config.language = 'fr';
                // config.uiColor = '#AADC6E';
        };
